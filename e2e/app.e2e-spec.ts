import { EventoWebAppPage } from './app.po';

describe('evento-web-app App', () => {
  let page: EventoWebAppPage;

  beforeEach(() => {
    page = new EventoWebAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
