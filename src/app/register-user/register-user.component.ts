///<reference path="../entities/user.ts"/>
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {matchValidator} from './matchValidate';
import {User} from '../entities/user';
import {Router} from '@angular/router';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {RegisterLoginService} from '../register-login.service';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {OidcService} from '../oidc.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  newUserForm: FormGroup;
  usernames: User[];
  isUsernameExists = false;
  isEmailExits = false;
  isPasswordRegExMatch = false;
  isPasswordsAreMatching = false;
  hasUpperCase = false;
  hasLowerCase = false;
  hasAtleastOneDigit = false;
  isMinLengthReached = false;
  upperCaseColor = 'red';
  lowerCaseColor = 'red';
  digitColor = 'red';
  minLengthColor = 'red';
  isTermsAndConditionsAccepted = false;

  constructor(private router: Router,
              private registerLogin: RegisterLoginService,
              public dialogRef: MdDialogRef<RegisterUserComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              private oidcService: OidcService) {
  }

  ngOnInit() {
    this.getAllUsernames();
    this.newUserForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
      displayName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
      email: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
      confirmMail: new FormControl('', [Validators.required, matchValidator('email')]),
      password: new FormControl('', Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')),
      confirmPassword: new FormControl('', [Validators.required, matchValidator('password')]),
    });
  }

  onSubmit() {
    this.checkUsername();
    this.checkEmail();
    const newRegisteredUser = this.createUserFromObject(this.newUserForm.value);
    this.dialogRef.close(newRegisteredUser);
    this.hashUserPassword(newRegisteredUser);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  createUserFromObject(form: any): User {
    const newUser = new User();
    newUser.username = form.username;
    newUser.name = form.displayName;
    newUser.email = form.email;
    newUser.password = form.password;
    newUser.termsAccepted = this.isTermsAndConditionsAccepted;
    return newUser;
  }

  hashUserPassword(user: User) {

    user.password = btoa(user.password);
    this.registerLogin.registerUser(user).then((res) => {
      localStorage.setItem('Login', 'true');
      localStorage.setItem('displayName', res.name);
      sessionStorage.setItem('currentSession', 'true');
      localStorage.setItem('id', res.id);
      localStorage.setItem('profilePicture', res.pic);
      this.router.navigateByUrl('/events');
      window.location.reload();
    });

  }

  getAllUsernames() {
    this.registerLogin.getAllUsernames().then((resp) => this.usernames = resp);
  }

  checkUsername() {
    for (let i = 0; i < this.usernames.length; i++) {
      if (this.usernames[i].username === this.newUserForm.value.username) {
        this.isUsernameExists = true;
        break;
      } else {
        this.isUsernameExists = false;
      }
    }
  }

  checkEmail() {
    for (let i = 0; i < this.usernames.length; i++) {
      if (this.usernames[i].email === this.newUserForm.value.email) {
        this.isEmailExits = true;
        break;
      } else {
        this.isEmailExits = false;
      }
    }
  }

  googleLogIn() {
    this.oidcService.getAuthorizationCode();
  }

  isPatternMatched() {
    const regexUpperCase = new RegExp('[A-Z]');
    const regexLowerCase = new RegExp('[a-z]');
    const regexDigit = new RegExp('[0-9]');
    const regexMinLength = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    if (!regexUpperCase.test(this.newUserForm.value.password)) {
      this.upperCaseColor = 'red';
      this.hasUpperCase = true;
    } else {
      this.upperCaseColor = 'green';
      this.hasUpperCase = true;
    }
    if (!regexLowerCase.test(this.newUserForm.value.password)) {
      this.lowerCaseColor = 'red';
      this.hasLowerCase = true;
    } else {
      this.lowerCaseColor = 'green';
      this.hasLowerCase = true;
    }
    if (!regexDigit.test(this.newUserForm.value.password)) {
      this.digitColor = 'red';
      this.hasAtleastOneDigit = true;
    } else {
      this.digitColor = 'green';
      this.hasAtleastOneDigit = true;
    }
    if (!regexMinLength.test(this.newUserForm.value.password)) {
      this.minLengthColor = 'red';
      this.isMinLengthReached = true;
    } else {
      this.minLengthColor = 'green';
      this.isMinLengthReached = true;
    }
  }

  isPasswordsMatch() {
    if (!(this.newUserForm.value.password === this.newUserForm.value.confirmPassword)) {
      this.isPasswordsAreMatching = true;
    } else {
      this.isPasswordsAreMatching = false;
    }
  }

  hidePasswordHint() {
    const regexMinLength = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    if (regexMinLength.test(this.newUserForm.value.password)) {
      this.isMinLengthReached = false;
      this.hasAtleastOneDigit = false;
      this.hasLowerCase = false;
      this.hasUpperCase = false;
    }
  }

  acceptTermsAndConditions() {
    this.isTermsAndConditionsAccepted = !this.isTermsAndConditionsAccepted;
  }
}
