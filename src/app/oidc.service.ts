import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {User} from './entities/user';

@Injectable()
export class OidcService {
  private identityProvider = 'https://accounts.google.com/o/oauth2/v2/auth';
  private clientId = '258595392709-krm7os6hhr1i2bjgqld3rd3lepog2s7j.apps.googleusercontent.com';
  private headersURL = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private _http: Http) {
  }

  getAuthorizationCode() {
    const serviceUrl = `${this.identityProvider}?client_id=${this.clientId}&scope=openid%20profile%20email&response_type=code&redirect_uri=${window.location.origin}`;
    window.location.replace(serviceUrl);
  }

  sendAuthorizationCode(code: string) {
    return this._http.post(`/api/users/authorize?code=${code}&redirect_uri=${window.location.origin}`, null, {
      withCredentials: true
    })
      .map((resp) => resp.json())
      .toPromise();
  }

  isAuthenticated() {
    const login = localStorage.getItem('Login');
    if (login === 'true') {
      return true;
    } else {
      return false;
    }
  }

  logoutService() {
    return this._http.get('/api/logout', {
      withCredentials: true
    })
      .toPromise();
  }

  sendTermsAndConditionsAnswer(terms: string) {
    const body = new URLSearchParams();
    body.set('terms', terms);
    return this._http.post('api/users/termsAndConditions', body.toString(), {
      headers: this.headersURL
    }).toPromise();
  }

  isStillLogin() {
    const body = new URLSearchParams();
    body.set('id', localStorage.getItem('id'));
    return this._http.post('api/users/isStillLogin', body.toString(), {headers: this.headersURL}
    ).toPromise();
  }

  getCurrentUser(): Promise<User> {
    return this._http.get('api/users/currentUser', {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as User);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error.status);
    return Promise.reject(error.message || error);
  }
}
