import {SectionExpansion} from './section-expansion';
import {Event} from '../entities/event';

const MAX_EVENT_SIZE_PER_PAGE = 4;

export class EventSectionExpansion {

  static loadEvents(eventsForLoading: Event[]): SectionExpansion {
    const sectionExpansion = new SectionExpansion('More');

    sectionExpansion.limit = MAX_EVENT_SIZE_PER_PAGE;
    sectionExpansion.loaded = MAX_EVENT_SIZE_PER_PAGE;
    sectionExpansion.total = eventsForLoading.length;
    return sectionExpansion;
  }

}
