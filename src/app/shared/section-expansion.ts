export class SectionExpansion {
  limit: number;
  total: number;
  loaded: number;
  buttonText: string;
  hasMore: boolean;

  constructor(buttonText) {
    this.buttonText = buttonText;
    this.hasMore = true;
  }

  load(buttonText): void {
    if (this.hasMore) {
      this.loaded += this.limit;
      if (this.loaded >= this.total) {
        this.hasMore = false;
        this.buttonText = 'Hide';
      }
    } else {
      this.loaded = this.limit;
      this.hasMore = true;
      this.buttonText = buttonText;
    }
  }
}
