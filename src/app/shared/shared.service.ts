import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MessageService {
  private subject = new Subject<any>();

  sendFeedbackAdded(message: string) {
    this.subject.next();
  }

  getFeedback(): Observable<any> {
    return this.subject.asObservable();
  }

  sendDisplayNameAdded(message: string) {
    this.subject.next({text: message});
  }

  getDisplayName(): Observable<any> {
    return this.subject.asObservable();
  }

  sendClosedEventUpdate(message: string) {
    this.subject.next({text: message});
  }

  getClosedEventUpdate(): Observable<any> {
    return this.subject.asObservable();
  }
}

@Injectable()
export class FeedbackUpdatedService {
  private subject = new Subject<any>();

  sendFeedbackAdded(message: string) {
    this.subject.next();
  }

  getFeedback(): Observable<any> {
    return this.subject.asObservable();
  }

  sendClosedEventUpdate(message: string) {
    this.subject.next({text: message});
  }

  getClosedEventUpdate(): Observable<any> {
    return this.subject.asObservable();
  }
}

@Injectable()
export class CreateEventUpdateService {
  private subject = new Subject<any>();

  updateVisibility(message: string) {
    this.subject.next({text: message});
  }

  getNewVisibility(): Observable<any> {
    return this.subject.asObservable();
  }
}

