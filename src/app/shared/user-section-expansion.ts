import {SectionExpansion} from './section-expansion';
import {User} from '../entities/user';

const MAX_USERS_SIZE_PER_PAGE = 8;

export class UserSectionExpansion {

  static loadUsers(usersForLoading: User[]): SectionExpansion {
    const sectionExpansion = new SectionExpansion('More users');

    sectionExpansion.limit = MAX_USERS_SIZE_PER_PAGE;
    sectionExpansion.loaded = MAX_USERS_SIZE_PER_PAGE;
    sectionExpansion.total = usersForLoading.length;
    return sectionExpansion;
  }
}
