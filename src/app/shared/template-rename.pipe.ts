import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'templateRename'
})
export class TemplateRenamePipe implements PipeTransform {

  transform(value: string) {
    if (value === 'NEW') {
      return 'For new event';
    } else if (value === 'CHANGED') {
      return 'For changed event';
    } else if (value === 'FEEDBACK') {
      return 'For feedback';
    }
  }

}
