export class TimeZoneConverter {
  static convertToTimestamp(date: Date): number {
    const tzDifference = date.getTimezoneOffset();
    const offsetTime = new Date(date.getTime() + tzDifference * 60 * 1000);
    return offsetTime.getTime();
  }

  static convertToUTCDate(timestamp: number): Date {
    const timeDate: Date = new Date(timestamp);
    const tzDifference = timeDate.getTimezoneOffset();
    return new Date(timestamp - tzDifference * 60 * 1000);
  }
}
