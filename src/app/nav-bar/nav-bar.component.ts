import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {OidcService} from '../oidc.service';
import {User} from '../entities/user';
import {Subscription} from 'rxjs/Subscription'
import {CreateEventUpdateService} from '../shared/shared.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isLogin: string;
  isTermsAccepted: boolean;
  currentUser: User;
  pictureURL: string;
  currentUserName: string;
  subscription: Subscription;

  constructor(private _router: Router,
              private _zone: NgZone,
              private _oidcService: OidcService,
              private _sharedService: CreateEventUpdateService) {
  }

  ngOnInit() {
    this.pictureURL = localStorage.getItem('profilePicture');
    this.currentUserName = localStorage.getItem('displayName');
    this.currentUser = new User();
    this.isLogin = localStorage.getItem('Login');
    if (localStorage.getItem('Login')) {
      this._oidcService.getCurrentUser().then((user) => {
        this.currentUser = user;
        this.isTermsAccepted = user.termsAccepted;
      }).catch((error) => {
        if (error.status === 403 && localStorage.getItem('id') !== null) {
          this._oidcService.isStillLogin().then((resp) => this._oidcService.getCurrentUser().then((user) => {
            this.currentUser = user;
            this.isTermsAccepted = user.termsAccepted;
          }));
        }
      });
    }
    this.subscription = this._sharedService.getNewVisibility().subscribe(message => {
      this.isTermsAccepted = message.text;
    });
  }

  onHomeClick() {
    this._router.navigateByUrl('/');
  }

  onAdminPanelClick() {
    this._router.navigateByUrl('/admin-panel/groups');
  }

  openCalendar() {
    this._router.navigateByUrl('/calendar');
  }

}
