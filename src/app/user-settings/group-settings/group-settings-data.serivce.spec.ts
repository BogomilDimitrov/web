import {inject, TestBed} from '@angular/core/testing';

import {MockBackend, MockConnection} from '@angular/http/testing';
import {BaseRequestOptions, Http, RequestMethod, Response, ResponseOptions} from '@angular/http';
import {User} from '../../entities/user';
import {UserGroupSubscription} from './user-group-subscription';
import {Group} from '../../entities/group';
import {GroupSettingsDataService} from './group-settings-data.service';


describe('Get and update user notifications', () => {
  let subject: GroupSettingsDataService;
  let backend: MockBackend;
  const currentUser: User = {
    id: '1',
    name: 'Bob',
    username: 'Bob',
    password: null,
    email: 'bob@gmail.com',
    sub: '123',
    pictureURL: 'http://google.com/photo',
    role: 'USER',
    enableNotifications: true,
  };
  const group1: Group = {
    id: '1',
    name: 'Developers',
  };
  const group2: Group = {
    id: '2',
    name: 'Party',
  };
  const newGroupSubscriptions: UserGroupSubscription[] = [{
    user: currentUser,
    subscribedGroup: group1,
    notified: true,
    subscribed: true,
  }, {
    user: currentUser,
    subscribedGroup: group2,
    notified: true,
    subscribed: true,
  }];


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GroupSettingsDataService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, defaultOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  beforeEach(inject([GroupSettingsDataService, MockBackend], (groupDataService, mockBackend) => {
    subject = groupDataService;
    backend = mockBackend;
  }));

  it('should get all group settings for current user', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      const options = new ResponseOptions({body: newGroupSubscriptions});

      connection.mockRespond(new Response(options));
    });

    subject.getAllUserSubscribedGroups().then((response) => {
      expect(response).toEqual(newGroupSubscriptions);
      done();
    });
  });

  it('should update user group settings with proper arguments', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      expect(connection.request.url).toEqual(`/api/user-groups`);
      expect(connection.request.method).toEqual(RequestMethod.Patch);
      expect(connection.request.getBody()).toEqual(JSON.stringify(newGroupSubscriptions));
      const options = new ResponseOptions({status: 200});

      connection.mockRespond(new Response(options));
    });

    subject.updateGroupSettings(newGroupSubscriptions).then((response) => {
      expect(response.status).toBe(200);
      done();
    });
  });
});
