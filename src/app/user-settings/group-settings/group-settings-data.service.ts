import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {UserGroupSubscription} from './user-group-subscription';
import {Group} from '../../entities/group';

@Injectable()
export class GroupSettingsDataService {

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  getAllUserSubscribedGroups(): Promise<UserGroupSubscription[]> {
    return this.http
      .get(`/api/user-groups`, {
        withCredentials: true
      })
      .toPromise()
      .then((resp) => resp.json() as UserGroupSubscription[]);
  }

  getAllGroups(): Promise<Group[]> {
    return this.http
      .get(`/api/groups`, {
        withCredentials: true
      })
      .toPromise()
      .then((resp) => resp.json() as Group[]);
  }

  updateGroupSettings(userGroupSubscriptions: UserGroupSubscription[]): Promise<Response> {
    return this.http
      .patch(`/api/user-groups`, JSON.stringify(userGroupSubscriptions), {
          withCredentials: true, headers: this.headers
        }
      ).toPromise().catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
