import {Component, OnInit} from '@angular/core';
import {Group} from '../../entities/group';
import {UserGroupSubscription} from './user-group-subscription';
import {GroupSettingsDataService} from './group-settings-data.service';
import {OidcService} from '../../oidc.service';
import {Dir, MdSnackBar, MdSnackBarConfig, MdSnackBarVerticalPosition} from '@angular/material';

@Component({
  selector: 'app-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.css']
})
export class GroupSettingsComponent implements OnInit {

  groups: Group[];
  userGroupSubscription: UserGroupSubscription[] = [];
  verticalPosition: MdSnackBarVerticalPosition = 'top';


  constructor(private groupSettingsService: GroupSettingsDataService,
              private oidcService: OidcService,
              public snackBar: MdSnackBar, private dir: Dir) {

  }

  ngOnInit() {
    this.oidcService.isStillLogin();
    this.groupSettingsService.getAllGroups()
      .then((resp) => this.groups = resp);

    this.groupSettingsService.getAllUserSubscribedGroups()
      .then((resp) => this.userGroupSubscription = resp);
  }

  isUserSubscribedForGroup(group: Group): boolean {
    const groupSub = this.userGroupSubscription
      .filter(gr => (gr.subscribedGroup.name === group.name) && gr.subscribed
      )[0];

    return groupSub && groupSub.subscribed;
  }

  subscribeUserForGroup(group: Group) {
    let exist = false;
    this.userGroupSubscription.forEach(gr => {
      if (gr.subscribedGroup.name === group.name) {
        gr.subscribed = !gr.subscribed;
        if (!gr.subscribed) {
          gr.notified = false;
        }
        exist = true;
      }
    });
    if (!exist) {
      const userGroupSub: UserGroupSubscription = {
        user: null,
        subscribedGroup: group,
        notified: false,
        subscribed: true,
      };
      this.userGroupSubscription.push(userGroupSub);
    }
  }

  updateGroupSettings() {
    this.groupSettingsService.updateGroupSettings(this.userGroupSubscription).then((resp) => {

      this.openSnackBar('Changes saved.', '');
    }).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this.oidcService.isStillLogin().then((resp) => this.updateGroupSettings());
      }
    });
  }

  uncheckAllNotifications() {
    this.userGroupSubscription.forEach(gr => {
      gr.subscribed = false;
      gr.notified = false;
    });
  }

  checkAllNotifications() {
    this.groups.forEach(group => {
      this.subscribeUserForGroup(group);
    });
    this.userGroupSubscription.forEach(gr => {
      gr.subscribed = true;
      gr.notified = true;
    });
  }

  isUserSubscribedForNotification(group: Group): boolean {
    const groupSub = this.userGroupSubscription
      .filter(gr => (gr.subscribedGroup.name === group.name) && gr.notified
      )[0];
    return groupSub && groupSub.notified;
  }

  notifyUserForGroup(group: Group) {
    for (let i = 0; i < this.userGroupSubscription.length; i++) {
      if ((this.userGroupSubscription[i].subscribedGroup.name === group.name) && this.userGroupSubscription[i].subscribed) {
        this.userGroupSubscription[i].notified = !this.userGroupSubscription[i].notified;
      }
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MdSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.duration = 5000;
    config.direction = this.dir.value;
    this.snackBar.open(message, action, config);
  }
}
