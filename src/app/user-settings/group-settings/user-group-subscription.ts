import {User} from '../../entities/user';
import {Group} from '../../entities/group';

export class UserGroupSubscription {
  user: User;
  subscribedGroup: Group;
  notified: boolean;
  subscribed: boolean;
}
