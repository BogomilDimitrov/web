import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {ChangeEmail} from './change-email';

@Injectable()
export class PersonalDataService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private headersURL = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private http: Http, ) {

  }

  changeUserDetails(changeEmail: ChangeEmail, displayName) {
    const body = new URLSearchParams();
    body.set('newEmail', changeEmail.newEmail);
    body.set('displayName', displayName);
    return this.http
      .patch(`api/users/changeDetails/`, body.toString(), {
          withCredentials: true, headers: this.headersURL
        }
      )
      .toPromise().catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
