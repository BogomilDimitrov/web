import {Component, OnInit} from '@angular/core';
import {ChangeEmail} from './change-email';
import {PersonalDataService} from './personal-data.service';
import {Router} from '@angular/router';
import {OidcService} from '../../oidc.service';
import {User} from '../../entities/user';
import {RegisterLoginService} from '../../register-login.service';
import {MessageService} from '../../shared/shared.service';
import {Dir, MdSnackBar, MdSnackBarConfig, MdSnackBarVerticalPosition} from '@angular/material';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit {
  changedEmail: ChangeEmail;
  displayName: string;
  currentUser: User;
  usernames: User[];
  isEmailExists = false;
  verticalPosition: MdSnackBarVerticalPosition = 'top';


  constructor(private personalService: PersonalDataService,
              private router: Router,
              private oidcService: OidcService,
              private registerLogin: RegisterLoginService,
              private sharedService: MessageService,
              public snackBar: MdSnackBar, private dir: Dir) {
  }

  ngOnInit() {
    this.getAllUsernames();
    this.oidcService.isStillLogin();
    this.changedEmail = new ChangeEmail;
    this.currentUser = new User;
    this.oidcService.getCurrentUser().then((resp) => this.currentUser = resp).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this.oidcService.isStillLogin().then((resp) => this.oidcService.getCurrentUser().then((resp1) => this.currentUser = resp1));
      }
    });
  }

  private changeDetails(): void {
    this.personalService.changeUserDetails(this.changedEmail, this.displayName)
      .then((resp) => {

        if (this.displayName !== undefined) {
          localStorage.removeItem('displayName');
          localStorage.setItem('displayName', this.displayName);
          this.sharedService.sendDisplayNameAdded(this.displayName);
          this.displayName = undefined;

        }
        if (this.changedEmail.newEmail !== undefined) {
          this.currentUser.email = this.changedEmail.newEmail;
          this.changedEmail.newEmail = undefined;
        }
        this.openSnackBar('Changes Saved.', '');
      }).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this.oidcService.isStillLogin().then((resp) => this.changeDetails());
      }
    });
  }


  getAllUsernames() {
    this.registerLogin.getAllUsernames().then((resp) => this.usernames = resp).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this.oidcService.isStillLogin().then((resp) => this.getAllUsernames());
      }
    });
  }

  checkEmail() {
    for (let i = 0; i < this.usernames.length; i++) {

      if (this.usernames[i].email === this.changedEmail.newEmail) {
        this.isEmailExists = true;
        break;
      } else {
        this.isEmailExists = false;
      }
    }
  }

  openSnackBar(message: string, action: string) {
    const config = new MdSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.duration = 5000;
    config.direction = this.dir.value;
    this.snackBar.open(message, action, config);
  }
}
