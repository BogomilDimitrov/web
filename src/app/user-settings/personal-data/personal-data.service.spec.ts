import {inject, TestBed} from '@angular/core/testing';
import {PersonalDataService} from './personal-data.service';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {BaseRequestOptions, Http, RequestMethod, Response, ResponseOptions} from '@angular/http';
import {ChangeEmail} from './change-email';

describe('Update user Details', () => {
  let subject: PersonalDataService;
  let backend: MockBackend;
  const changedData = {
    currentEmail: 'bob@gmail.com',
    newEmail: 'bob123@gmail.com',
    displayName: 'Bob',
  };

  const changeEmail: ChangeEmail = {

    newEmail: 'bob123@gmail.com',
  };
  const displayName = 'Bob';

  const body = new URLSearchParams();
  body.set('currentEmail', 'bob@gmail.com');
  body.set('newEmail', 'bob123@gmail.com');
  body.set('displayName', 'Bob');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PersonalDataService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, defaultOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  beforeEach(inject([PersonalDataService, MockBackend], (notificationService, mockBackend) => {
    subject = notificationService;
    backend = mockBackend;
  }));

  it('should update user Personal data', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      expect(connection.request.url).toEqual(`api/users/changeDetails/`);
      expect(connection.request.method).toEqual(RequestMethod.Patch);
      expect(connection.request.headers.get('Content-Type')).toEqual('application/x-www-form-urlencoded');
      expect(connection.request.getBody()).toEqual(body.toString());
      const options = new ResponseOptions({status: 200});

      connection.mockRespond(new Response(options));
    });

    subject.changeUserDetails(changeEmail, displayName).then((response) => {
      expect(response.status).toBe(200);
      done();
    });
  });
});
