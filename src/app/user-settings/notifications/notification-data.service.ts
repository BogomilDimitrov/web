import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {MailSubscriber} from './mail-subscriber';

@Injectable()
export class NotificationDataService {

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  getAllNotificationTypesForUser(): Promise<MailSubscriber[]> {
    return this.http
      .get(`/api/userNotifications`, {
        withCredentials: true
      })
      .toPromise()
      .then((resp) => resp.json() as MailSubscriber[]);
  }

  updateAllNotificationType(mailSubscribers: MailSubscriber[]): Promise<Response> {
    return this.http
      .patch(`/api/editNotifications`, JSON.stringify(mailSubscribers), {
          withCredentials: true, headers: this.headers
        }
      ).toPromise().catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
