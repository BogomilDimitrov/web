import {MailTemplate} from '../../entities/mail-template';
import {User} from '../../entities/user';

export class MailSubscriber {
  user: User;
  mailTemplates: MailTemplate;
  enabled: boolean;

  constructor() {
    this.user = new User();
    this.mailTemplates = new MailTemplate();
  }
}
