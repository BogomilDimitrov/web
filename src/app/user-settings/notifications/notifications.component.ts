import {Component, OnInit} from '@angular/core';
import {NotificationDataService} from './notification-data.service';
import {MailSubscriber} from './mail-subscriber';
import {OidcService} from '../../oidc.service';
import {Dir, MdSnackBar, MdSnackBarConfig, MdSnackBarVerticalPosition} from '@angular/material';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
})
export class NotificationsComponent implements OnInit {

  verticalPosition: MdSnackBarVerticalPosition = 'top';
  userNotifications: MailSubscriber[];
  allNotificationStatus: object;

  constructor(private notifyData: NotificationDataService,
              private oidcService: OidcService,
              public snackBar: MdSnackBar, private dir: Dir) {
  }

  ngOnInit() {
    this.oidcService.isStillLogin();
    this.notifyData.getAllNotificationTypesForUser()
      .then(userNotifications => {
        this.userNotifications = userNotifications;
      });
  }

  changeStatusForSubscriber(subscriber: MailSubscriber) {
    const notification = this.userNotifications.filter(sub => sub === subscriber)[0];
    notification.enabled = !notification.enabled;
  }

  updateMailNotificationSettings(): void {
    this.notifyData.updateAllNotificationType(this.userNotifications)
      .then(resp => this.openSnackBar('Changes Saved.', '')).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this.oidcService.isStillLogin().then((resp) => this.updateMailNotificationSettings());
      }
    });
  }

  uncheckAllNotifications() {
    this.userNotifications.forEach(sub => sub.enabled = false);
  }

  checkAllNotifications() {
    this.userNotifications.forEach(sub => sub.enabled = true);
  }

  openSnackBar(message: string, action: string) {
    const config = new MdSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.duration = 5000;
    config.direction = this.dir.value;
    this.snackBar.open(message, action, config);
  }
}
