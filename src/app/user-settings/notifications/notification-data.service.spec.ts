import {inject, TestBed} from '@angular/core/testing';

import {NotificationDataService} from './notification-data.service';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {BaseRequestOptions, Http, RequestMethod, Response, ResponseOptions} from '@angular/http';
import {MailSubscriber} from './mail-subscriber';
import {User} from '../../entities/user';
import {MailTemplate} from '../../entities/mail-template';

describe('NotificationDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationDataService]
    });
  });
});


describe('Get and update user notifications', () => {
  let subject: NotificationDataService;
  let backend: MockBackend;
  const currentUser: User = {
    id: '1',
    username: null,
    password: null,
    name: 'Bob',
    email: 'bob@gmail.com',
    sub: '123',
    pictureURL: 'http://google.com/photo',
    role: 'USER',
    enableNotifications: true,
  };

  const newMailTemplate: MailTemplate = {
    id: '1',
    subject: 'New',
    filePath: 'C:/templates/new.html',
    type: 'NEW',
  };

  const chageMailTemplate: MailTemplate = {
    id: '2',
    subject: 'Change',
    filePath: 'C:/templates/new.html',
    type: 'CHANGE',
  };

  const userNotifications: MailSubscriber[] = [{
    user: currentUser,
    mailTemplates: newMailTemplate,
    enabled: true,
  }, {
    user: currentUser,
    mailTemplates: chageMailTemplate,
    enabled: true,
  }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NotificationDataService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, defaultOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  beforeEach(inject([NotificationDataService, MockBackend], (notificationService, mockBackend) => {
    subject = notificationService;
    backend = mockBackend;
  }));

  it('should get all notifications for current user', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      const options = new ResponseOptions({body: userNotifications});

      connection.mockRespond(new Response(options));
    });

    subject.getAllNotificationTypesForUser().then((response) => {
      expect(response).toEqual(userNotifications);
      done();
    });
  });

  it('should update user notifications with proper arguments', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      expect(connection.request.url).toEqual(`/api/editNotifications`);
      expect(connection.request.method).toEqual(RequestMethod.Patch);
      expect(connection.request.getBody()).toEqual(JSON.stringify(userNotifications));
      const options = new ResponseOptions({status: 200});

      connection.mockRespond(new Response(options));
    });

    subject.updateAllNotificationType(userNotifications).then((response) => {
      expect(response.status).toBe(200);
      done();
    });
  });
});
