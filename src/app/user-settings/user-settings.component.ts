import {Subscription} from 'rxjs/Subscription';
import {MessageService} from '../shared/shared.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {
  pictureURL: string;
  currentUserName: string;
  subscription: Subscription;

  constructor(private sharedService: MessageService) {
    this.pictureURL = localStorage.getItem('profilePicture');
    this.currentUserName = localStorage.getItem('displayName');
    this.subscription = this.sharedService.getDisplayName().subscribe(message => {
      this.currentUserName = message.text;
    });
  }

  ngOnInit() {
  }

}
