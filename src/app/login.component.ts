import {ApplicationRef, ChangeDetectionStrategy, Compiler, Component, NgZone, OnInit} from '@angular/core';
import {OidcService} from './oidc.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/switchMap';
import {RegisterUserComponent} from './register-user/register-user.component';
import {MdDialog} from '@angular/material';
import {User} from './entities/user';
import {SignInComponent} from './sign-in/sign-in.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class LoginComponent implements OnInit {

  user: User;
  isLogin: string;
  currentUserName: string;
  newUserName: string;
  pictureURL: string;

  constructor(private oidc: OidcService,
              private activeRoute: ActivatedRoute,
              private router: Router,
              public dialog: MdDialog,
              private appref: ApplicationRef,
              private ngZone: NgZone,
              private _compiler: Compiler) {
  }

  ngOnInit(): void {
    this.currentUserName = localStorage.getItem('displayName');
    this.newUserName = localStorage.getItem('displayName');
    this.isLogin = localStorage.getItem('Login');
    this.pictureURL = localStorage.getItem('profilePicture');
    this.activeRoute.queryParams.subscribe(
      (params: Params) => {
        const code = params['code'];
        if (code) {
          this.oidc.sendAuthorizationCode(code)
            .then((res) => {
              localStorage.setItem('Login', 'true');
              localStorage.setItem('displayName', res.name);
              sessionStorage.setItem('currentSession', 'true');
              localStorage.setItem('id', res.id);
              localStorage.setItem('profilePicture', res.pic);
              this.router.navigateByUrl('events');
              window.location.reload();
            });
        }
      }
    );
  }

  public login() {
    this.oidc.getAuthorizationCode();
  }

  public logout() {
    this.currentUserName = null;
    this.isLogin = null;
    this.pictureURL = null;
    localStorage.removeItem('Login');
    localStorage.removeItem('displayName');
    localStorage.removeItem('profilePicture');
    localStorage.removeItem('id');
    sessionStorage.removeItem('currentSession');
    this.oidc.logoutService().then(res => {
      window.location.reload();
    });
  }

  openDialogSignUp(): void {
    const dialogRef = this.dialog.open(RegisterUserComponent, {
      width: '45%',
      data: {user: this.user}
    });
  }

  openDialogSignIn(): void {
    const dialogRef = this.dialog.open(SignInComponent, {
      width: '35%',
    });
  }
}
