import {async, inject, TestBed} from '@angular/core/testing';

import {OidcService} from './oidc.service';
import {
  BaseRequestOptions,
  Http,
  HttpModule,
  RequestMethod,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {User} from './entities/user';

describe('OidcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OidcService]
    });
  });

});
describe('should send authorization code', () => {
  let subject: OidcService;
  let backend: MockBackend;
  const code = 'test';
  const responseForm = {
    uri: window.location.origin,
    pic: 'http://photos.google.com'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OidcService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (mockBackend, defaultOptions) => {
            return new Http(mockBackend, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  beforeEach(inject([OidcService, MockBackend], (oidc, mockBackend) => {
    subject = oidc;
    backend = mockBackend;
  }));

  it('should be called with proper arguments', (done) => {
    backend.connections.subscribe((connection: MockConnection) => {
      expect(connection.request.url).toEqual(`/api/users/authorize?code=${code}&redirect_uri=${window.location.origin}`);
      expect(connection.request.method).toEqual(RequestMethod.Post);
      expect(connection.request.getBody()).toEqual((null));
      const options = new ResponseOptions({
        body: responseForm
      });

      connection.mockRespond(new Response(options));
    });

    subject.sendAuthorizationCode(code).then((response) => {
      expect(response).toEqual(responseForm);
      done();
    });
  });
});

beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpModule],
    providers: [
      OidcService,
      {provide: XHRBackend, useClass: MockBackend}
    ]
  });
});
describe('getCurrentUser()', () => {
  const testUser: User = {
    username: null,
    password: null,
    id: '7e37us3r',
    name: 'Test User',
    email: 'test.user@gmail.com',
    sub: 'sub',
    pictureURL: '',
    role: 'USER',
    enableNotifications: false
  };

  it('should get current user', async(inject([OidcService, XHRBackend],
    (oidcService, mockBackend) => {
      let lastConnection;
      mockBackend.connections.subscribe((connection) => {
        lastConnection = connection;
        connection.mockRespond(new Response(new ResponseOptions({
          body: [testUser]
        })));
      });
      oidcService.getCurrentUser().then((response: User[]) => {
        expect(lastConnection.request.url).toMatch('api/users/currentUser', 'url not valid');
        expect(lastConnection.request.withCredentials).toEqual(true);
        expect(response[0]).toEqual(testUser);
      });
    })));
});
