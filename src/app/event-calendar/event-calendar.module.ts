import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule, CalendarModule, CheckboxModule, CodeHighlighterModule, DialogModule, InputTextModule,
  ScheduleModule, TabViewModule
} from 'primeng/primeng';
import {EventCalendarComponent} from './event-calendar.component';
import {EventService} from '../dashboard/events/event.service';

@NgModule({
  imports: [
    CommonModule,
    ScheduleModule,
    DialogModule,
    InputTextModule,
    CalendarModule,
    CheckboxModule,
    ButtonModule,
    TabViewModule,
    CodeHighlighterModule,
  ],
  declarations: [
    EventCalendarComponent
  ],
  providers: [
    EventService
  ]
})
export class EventCalendarModule {
}
