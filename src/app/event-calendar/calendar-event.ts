import { User } from './../entities/user';
export class CalendarEvent {
  id: string;
  title: string;
  start: string;
  backgroundColor: string;
  creator: User;
  eventColor: string;
  textColor: string;
  borderColor: string;
}
