import {User} from '../entities/user';
import {OidcService} from '../oidc.service';
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Event} from '../entities/event';
import {EventService} from '../dashboard/events/event.service';
import {CalendarEvent} from './calendar-event';
import {TimeZoneConverter} from '../shared/time-zone-converter';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event-calendar',
  templateUrl: './event-calendar.component.html',
  styleUrls: ['./event-calendar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EventCalendarComponent implements OnInit {

  calendarEvents: CalendarEvent[];
  public fnEvRender;
  currentUser: User;

  constructor(private _service: EventService, private _router: Router, private _oidc: OidcService) {
  }

  ngOnInit() {
    this.currentUser = new User;
    this._oidc.getCurrentUser().then((user) => this.currentUser = user);
    this._service.getMyEvents()
      .then((response) => this.transformEventsToCalendar(response));
  }

  transformEventsToCalendar(events: Event[]) {
    this.calendarEvents = [];
    for (const e of events) {
      const ce = new CalendarEvent();
      ce.title = e.name;
      ce.id = e.id;
      if (e.creator.email === this.currentUser.email) {
        ce.backgroundColor = '#9575CD';
        ce.textColor = '#fdfeff';
        ce.borderColor = '#9575CD';
      } else if (e.subscriptionType === 'GOING') {
        ce.backgroundColor = '#9CCC65';
        ce.textColor = '#fdfeff';
        ce.borderColor = '#9CCC65';
      } else if (e.subscriptionType === 'INTERESTED') {
        ce.backgroundColor = '#00BCD4';
        ce.textColor = '#fdfeff';
        ce.borderColor = '#00BCD4';
      }
      ce.start = TimeZoneConverter.convertToUTCDate(e.date).toLocaleString();
      this.calendarEvents.push(ce);
    }
  }

  goToEvent(event) {
    this._router.navigateByUrl(`/events/${event.calEvent.id}`);
  }
}
