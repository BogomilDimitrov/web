import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {User} from './entities/user';

@Injectable()
export class RegisterLoginService {
  private headersURL = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private _http: Http) {
  }


  registerUser(user: User) {
    return this._http.post(`/api/users/registerUser`, user, {
      withCredentials: true
    })
      .map((resp) => resp.json())
      .toPromise();
  }

  signIn(user: User) {
    return this._http.post(`/api/users/signIn`, user, {
      withCredentials: true
    })
      .map((resp) => resp.json())
      .toPromise();
  }

  getAllUsernames() {
    return this._http.get(`/api/users/getAll`)
      .toPromise()
      .then(response => response.json() as User[]);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
