import {Component, OnInit} from '@angular/core';
import {OidcService} from '../oidc.service';
import {Router} from '@angular/router';
import {CreateEventUpdateService} from "../shared/shared.service";

@Component({
  selector: 'app-accept-terms-and-conditions',
  templateUrl: './accept-terms-and-conditions.component.html',
  styleUrls: ['./accept-terms-and-conditions.component.css']
})
export class AcceptTermsAndConditionsComponent implements OnInit {

  constructor(private oidcService: OidcService,
              private router: Router,
              private sharedService: CreateEventUpdateService) {
  }

  ngOnInit() {
  }


  acceptTermsAndConditions() {
    this.oidcService.sendTermsAndConditionsAnswer('true').then((resp) => {
      localStorage.setItem('terms', 'true');
      this.sharedService.updateVisibility('true');
      this.router.navigateByUrl('');
    });
  }

  rejectTermsAndConditions() {
    localStorage.removeItem('Login');
    localStorage.removeItem('displayName');
    localStorage.removeItem('profilePicture');
    localStorage.removeItem('id');
    localStorage.removeItem('terms');
    sessionStorage.removeItem('currentSession');
    this.oidcService.logoutService().then(res => {
      window.location.reload();
    });
  }
}
