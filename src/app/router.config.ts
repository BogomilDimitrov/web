import {AuthGuard} from './auth-guard';
import {EventViewComponent} from './event-view/event-view.component';
import {Route, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {NgModule} from '@angular/core';
import {PersonalDataComponent} from './user-settings/personal-data/personal-data.component';
import {NotificationsComponent} from './user-settings/notifications/notifications.component';
import {GroupSettingsComponent} from './user-settings/group-settings/group-settings.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {UserManagementComponent} from './admin-panel/user-management/user-management.component';
import {GroupManagementComponent} from './admin-panel/group-management/group-management.component';
import {UploadTemplateComponent} from './admin-panel/upload-template/upload-template.component';
import {SettingsNavComponent} from './user-settings/settings-nav/settings-nav.component';
import {InternalErrorComponent} from './internal-error/internal-error.component';
import {BadRequestPageComponent} from './bad-request-page/bad-request-page.component';
import {NotFoundPageComponent} from './not-found-page/not-found-page.component';
import {EventCalendarComponent} from './event-calendar/event-calendar.component';
import {TermsAndConditionsComponent} from './terms-and-conditions/terms-and-conditions.component';
import {AcceptTermsAndConditionsComponent} from './accept-terms-and-conditions/accept-terms-and-conditions.component';

const routes: Route[] = [
  {path: '', component: HomeComponent},
  {path: 'calendar', component: EventCalendarComponent, canActivate: [AuthGuard]},
  {path: 'events/:id', component: EventViewComponent, canActivate: [AuthGuard]},
  {path: 'settings', redirectTo: 'settings/notifications', pathMatch: 'full'},
  {path: 'internal-error', component: InternalErrorComponent, canActivate: [AuthGuard]},
  {path: 'bad-request', component: BadRequestPageComponent, canActivate: [AuthGuard]},
  {path: 'page-not-found', component: NotFoundPageComponent, canActivate: [AuthGuard]},
  {path: 'termsAndConditions', component: TermsAndConditionsComponent},
  {path: 'acceptTermsAndConditions', component: AcceptTermsAndConditionsComponent},

  {
    path: 'admin-panel', component: AdminPanelComponent, canActivate: [AuthGuard],
    children: [
      {
        path: 'upload',
        component: UploadTemplateComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'users',
        component: UserManagementComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'groups',
        component: GroupManagementComponent,
        canActivateChild: [AuthGuard]
      }
    ]
  },

  {
    path: 'settings', component: SettingsNavComponent, canActivate: [AuthGuard],
    children: [
      {path: 'profile', component: PersonalDataComponent, canActivateChild: [AuthGuard]},
      {
        path: 'notifications',
        component: NotificationsComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'subscribe-group',
        component: GroupSettingsComponent,
        canActivateChild: [AuthGuard]
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
