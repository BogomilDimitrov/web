import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RegisterLoginService} from '../register-login.service';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {User} from '../entities/user';
import {OidcService} from '../oidc.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  isLoginSuccessed = false;
  logInUser: FormGroup;

  constructor(private router: Router,
              private registerLoginService: RegisterLoginService,
              public dialogRef: MdDialogRef<SignInComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              private oidcService: OidcService) {
  }

  ngOnInit() {
    this.logInUser = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    const newRegisteredUser = this.createUserLogInObject(this.logInUser.value);
    // this.dialogRef.close(newRegisteredUser);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  createUserLogInObject(form: any) {
    const logInUser = new User();
    logInUser.username = form.username;
    logInUser.password = btoa(form.password);
    this.registerLoginService.signIn(logInUser).then((res) => {
      localStorage.setItem('Login', 'true');
      localStorage.setItem('displayName', res.name);
      sessionStorage.setItem('currentSession', 'true');
      localStorage.setItem('id', res.id);
      localStorage.setItem('profilePicture', res.pic);
      this.router.navigateByUrl('events');
      window.location.reload();
    }).catch((resp) =>
      this.isLoginSuccessed = true
    );
  }

  googleLogIn() {
    this.oidcService.getAuthorizationCode();
  }
}
