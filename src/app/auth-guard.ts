import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {OidcService} from './oidc.service';
import {Observable} from 'rxjs/Observable';
import {User} from './entities/user';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(protected router: Router, protected oidcService: OidcService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    if (state.url !== '/' && !this.oidcService.isAuthenticated()) {
      this.router.navigateByUrl('');
      return false;
    }
    if (localStorage.getItem('Login')) {
      return this.oidcService.getCurrentUser()
        .then((user) => {
            if (localStorage.getItem('Login') && (state.url === '/admin-panel'
                || state.url === '/admin-panel/users'
                || state.url === '/admin-panel/upload'
                || state.url === '/admin-panel/groups')
              && user.role !== 'ADMIN') {
              this.router.navigateByUrl('');
              return false;
            }
            if (user.termsAccepted === false) {
              this.router.navigateByUrl('acceptTermsAndConditions');
            }
            return true;
          }
        ).catch((resp) => {
          this.oidcService.isStillLogin().then((res) => {
          });
        });
    }
  }
}

