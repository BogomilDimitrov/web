import {Component, OnInit} from '@angular/core';
import {OidcService} from '../oidc.service';
import {SectionExpansion} from '../shared/section-expansion';
import {EventSectionExpansion} from '../shared/events-section-expansion';
import {EventService} from '../dashboard/events/event.service';
import {Event} from '../entities/event';
import {Router} from "@angular/router";
import {User} from "../entities/user";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isTermsAccepted: boolean;
  isStillLogin = localStorage.getItem('Login');
  eventsSectionExpansion: SectionExpansion;
  private currentSession = sessionStorage.getItem('currentSession');

  eventsThatWillBeDisplayOnlyIfThereIsNotUser: Event[];

  constructor(private _oidcService: OidcService,
              private _eventService: EventService,
              private _router: Router) {
    this.eventsThatWillBeDisplayOnlyIfThereIsNotUser = [];

  }

  ngOnInit() {
    this.eventsSectionExpansion = new SectionExpansion('Events');
    if (this.currentSession !== 'true' && this.isStillLogin === 'true') {
      this._oidcService.isStillLogin().then((res) => {
        }
      );
    }
    if (this.isStillLogin === 'true') {
      this._oidcService.getCurrentUser().then((resp) => {
        this.isTermsAccepted = resp.termsAccepted;
        if (this.isTermsAccepted === false) {
          this._router.navigateByUrl('acceptTermsAndConditions');
        }
      });
    }
    this._eventService.getEventsWithLimit(4)
      .then((result) => {
        this.eventsThatWillBeDisplayOnlyIfThereIsNotUser = result;
        this.eventsSectionExpansion = EventSectionExpansion.loadEvents(this.eventsThatWillBeDisplayOnlyIfThereIsNotUser);
      });
  }
}
