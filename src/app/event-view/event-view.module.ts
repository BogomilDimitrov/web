import {CapitalizePipe} from '../shared/capitalize.pipe';
import {RemoveUnderscorePipe} from '../shared/remove-underscore.pipe';
import {StatusMenuComponent} from './event-details/status-menu/status-menu.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EventViewComponent} from './event-view.component';
import {EventDetailsComponent} from './event-details/event-details.component';
import {StarsComponent} from './stars/stars.component';
import {SubscriptionButtonsComponent} from './event-details/subscription-buttons/subscription-buttons.component';
import {MaterialModule} from '../material/material.module';
import {EventService} from '../dashboard/events/event.service';
import {CommentSectionComponent} from './comment-section/comment-section.component';
import {CommentComponent} from './comment-section/comment/comment.component';
import {AddCommentComponent} from './comment-section/add-comment/add-comment.component';
import {EditEventComponent} from './edit-event/edit-event.component';
import {EditEventDialogComponent} from './edit-event/edit-event-dialog/edit-event-dialog.component';
import {FeedbackComponent} from './feedback-section/feedback/feedback.component';
import {AddFeedbackComponent} from './feedback-section/add-feedback/add-feedback.component';
import {FeedbackSectionComponent} from './feedback-section/feedback-section.component';
import {FeedbackService} from './feedback-section/feedback.service';
import {UploadPictureComponent} from './event-details/upload-picture/upload-picture.component';
import {UploadPictureService} from './event-details/upload-picture/upload-picture.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    EventViewComponent,
    CommentSectionComponent,
    CommentComponent,
    AddCommentComponent,
    StatusMenuComponent,
    FeedbackComponent
  ],
  declarations: [
    EventViewComponent,
    EventDetailsComponent,
    StarsComponent,
    SubscriptionButtonsComponent,
    EditEventComponent,
    EditEventDialogComponent,
    CommentSectionComponent,
    CommentComponent,
    AddCommentComponent,
    StatusMenuComponent,
    RemoveUnderscorePipe,
    CapitalizePipe,
    UploadPictureComponent,
    CapitalizePipe,
    FeedbackSectionComponent,
    FeedbackComponent,
    AddFeedbackComponent,
  ],
  providers: [
    EventService,
    FeedbackService,
    UploadPictureService
  ],
  bootstrap: [
    EventViewComponent
  ],
  entryComponents: [
    EditEventDialogComponent
  ]
})
export class EventViewModule {
}
