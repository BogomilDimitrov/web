import {Component, Input, OnInit} from '@angular/core';
import {EventService} from '../../dashboard/events/event.service';
import {Comment} from '../../entities/comment';
import {ActivatedRoute} from '@angular/router';
import {SectionExpansion} from '../../shared/section-expansion';
import {TimeZoneConverter} from '../../shared/time-zone-converter';

@Component({
  selector: 'app-comment-section',
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.css']
})
export class CommentSectionComponent implements OnInit {
  @Input() currentEventId: string;
  comments: Comment[] = [];
  commentSectionExpansion: SectionExpansion;
  sub: any;


  constructor(private _service: EventService,
              private _route: ActivatedRoute) {
  }

  ngOnInit() {
    this.commentSectionExpansion = new SectionExpansion('More comments');
    this.sub = this._route.params.subscribe(params => {
      this.currentEventId = params['id'];
    });
    this._service.getAllComments(this.currentEventId)
      .then((comments) => {
        this.comments = comments;
        this.commentSectionExpansion.limit = 5;
        this.commentSectionExpansion.loaded = 5;
        this.commentSectionExpansion.total = this.comments.length;
        this.comments.forEach((comment) =>
          comment.dateOfCreation = TimeZoneConverter.convertToUTCDate(comment.dateOfCreation).getTime());
      });
  }

  commentEmitted() {
    this._service.getAllComments(this.currentEventId).then((result) => {
      this.comments = result;
      this.commentSectionExpansion.total = this.comments.length;
    });
  }
}
