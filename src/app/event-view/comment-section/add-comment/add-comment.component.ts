import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventService} from '../../../dashboard/events/event.service';
import {Router} from '@angular/router';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})

export class AddCommentComponent {
  @Input() currentEventId: string;
  @Output() newCommentAdded = new EventEmitter<Comment>();
  commentForm: FormGroup;
  commentContent: string;
  inFocus = false;
  commentContentMinLength = 2;

  constructor(private _formBuilder: FormBuilder,
              private _service: EventService,
              private _router: Router) {
    this.initForm();
  }

  initForm() {
    this.commentForm = this._formBuilder.group({
      commentContent: ['', Validators.compose([Validators.minLength(this.commentContentMinLength)])]
    });
  }

  isEmptyCommentContext(): boolean {
    if (isNullOrUndefined(this.commentForm.value.commentContent)) {
      return true;
    }
    this.commentContent = this.commentForm.value.commentContent.trim();
    return this.commentContent === '' || this.commentContent.length < this.commentContentMinLength;
  }

  addComment() {
    this.commentContent = this.commentForm.value.commentContent.trim();
    this._service.postAComment(this.commentContent, this.currentEventId)
      .then(() => this.newCommentAdded.emit())
      .catch((error) => {
        if (error.status === 400) {
          this._router.navigateByUrl('bad-request');
        }
      });
    this.commentForm.reset();
  }
}
