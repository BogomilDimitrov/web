import {Component, Input, OnInit} from '@angular/core';
import {EventService} from '../../dashboard/events/event.service';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit {
  @Input() rating: number;

  constructor(private _service: EventService) {
  }

  ngOnInit(): void {
  }

  getRating() {
  }
}
