import {Subscription} from 'rxjs/Subscription';
import {FeedbackUpdatedService} from '../shared/shared.service';
import {Feedback} from '../entities/feedback';

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../dashboard/events/event.service';
import {Event} from '../entities/event';
import {FeedbackService} from './feedback-section/feedback.service';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {
  private sub: any;
  id: string;
  currentEvent: Event;
  private subscription: Subscription;
  feedback: Feedback;

  constructor(private _service: EventService,
              private _route: ActivatedRoute,
              private _router: Router,
              private _feedbackService: FeedbackService,
              private _sharedService: FeedbackUpdatedService) {
    this.sub = this._route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getFeedbacks();
    this.feedback = {
      rating: 0,
      feedbacks: []
    };
    this.subscription = this._sharedService.getFeedback().subscribe(message => {
      this.getFeedbacks();
    });
  }

  ngOnInit() {
    this.currentEvent = new Event();
    this.getEvent();
    this.getFeedbacks();
  }

  getEvent(): void {
    this._service.getEventById(this.id).then(event => {
      this.currentEvent = event;
    }).catch((error) => {
      if (error.status === 404) {
        this._router.navigateByUrl('page-not-found');
      }
    });
  }

  getFeedbacks() {
    this._feedbackService.getAllReviews(this.id)
      .then((result) => {
        this.feedback = result;
      })
      .catch((error) => {
          if (error.status === 404) {
            this._router.navigateByUrl('page-not-found');
          }
        }
      );
  }
}
