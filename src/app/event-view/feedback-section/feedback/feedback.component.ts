import {EventSubscriber} from '../../../entities/event-subscriber';
import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  @Input() feedback: EventSubscriber;
  starArr: number[] = [];

  constructor() {
    this.feedback = new EventSubscriber();
  }

  ngOnInit() {
    this.starArr = Array<number>(this.feedback.feedbackRating).fill(1);
  }
}
