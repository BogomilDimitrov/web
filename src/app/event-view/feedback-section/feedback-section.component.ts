import {Component, Input, OnInit} from '@angular/core';
import {EventSubscriber} from '../../entities/event-subscriber';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-feedback-section',
  templateUrl: './feedback-section.component.html',
  styleUrls: ['./feedback-section.component.css']
})
export class FeedbackSectionComponent implements OnInit {
  @Input() feedbacks: EventSubscriber[] = [];
  isFeedbackAvailable = false;
  currentEventId;
  sub: any;

  constructor(private _route: ActivatedRoute) {
    this.feedbacks = [];
  }

  ngOnInit() {
    this.sub = this._route.params.subscribe(params => {
      this.currentEventId = params['id'];
    });
  }
}
