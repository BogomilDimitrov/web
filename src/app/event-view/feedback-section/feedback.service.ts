import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {Feedback} from '../../entities/feedback';

const FEEDBACK_BASE_URL = '/api/feedback/';

@Injectable()
export class FeedbackService {
  constructor(private _http: Http) {
  }

  getAllReviews(id: string): Promise<Feedback> {
    return this._http.get(FEEDBACK_BASE_URL + id, {
      withCredentials: true
    }).toPromise()
      .then((response) => response.json() as Feedback)
      .catch(this.handleError);
  }

  addFeedback(eventId: string, comment: string, rating: number): Promise<any> {
    const body = new URLSearchParams();
    body.set('eventId', eventId);
    body.set('rating', rating.toString());
    body.set('comment', comment);
    return this._http.patch(FEEDBACK_BASE_URL, body, {
      withCredentials: true
    }).toPromise();
  }

  checkFeedbackAvailability(eventId: string): Promise<any> {
    const url = FEEDBACK_BASE_URL + eventId + '/feedbackAvailable';
    return this._http.get(url, {
      withCredentials: true
    }).toPromise()
      .then(response => response.json());
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
