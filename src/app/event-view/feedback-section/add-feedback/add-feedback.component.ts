import {FeedbackUpdatedService} from '../../../shared/shared.service';
import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MdSnackBar} from '@angular/material';
import {FeedbackService} from '../feedback.service';
import {OidcService} from '../../../oidc.service';
import {isNullOrUndefined} from 'util';

const MIN_LENGTH = 2;

@Component({
  selector: 'app-add-feedback',
  templateUrl: './add-feedback.component.html',
  styleUrls: ['./add-feedback.component.css']
})

export class AddFeedbackComponent implements OnInit {

  @Input() currentEventId: string;
  formDisabled: boolean;
  inFocus = false;
  feedbackForm: FormGroup;
  feedbackContent: string;
  rating = 0;

  constructor(public snackBar: MdSnackBar,
              private _formBuilder: FormBuilder,
              private _feedbackService: FeedbackService,
              private router: Router,
              private _oidcService: OidcService,
              private _sharedService: FeedbackUpdatedService) {
    this.initForm();
    this._sharedService.getClosedEventUpdate().subscribe(message => {
      this.checkFeedbackFormAvailability();
    });
  }

  initForm() {
    this.feedbackForm = this._formBuilder.group({
      feedbackContent: ['', Validators.compose([Validators.minLength(MIN_LENGTH)])],
      feedbackRating: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.checkFeedbackFormAvailability();
  }

  formDisable() {
    this.formDisabled = false;
  }

  addFeedback() {
    this.feedbackContent = this.feedbackForm.value.feedbackContent.trim();
    this.rating = this.feedbackForm.value.feedbackRating;
    this._feedbackService.addFeedback(this.currentEventId, this.feedbackContent, this.rating)
      .then(() => {
        this.feedbackForm.reset();
        this.checkFeedbackFormAvailability();
        this._sharedService.sendFeedbackAdded('');
      })
      .catch((error) => {
        if (error.status === 403 && localStorage.getItem('id') !== null) {
          this._oidcService.isStillLogin()
            .then((resp) => window.location.reload());
        }
      });
  }

  isEmptyCommentContext(): boolean {
    if (isNullOrUndefined(this.feedbackForm.value.feedbackContent)) {
      return true;
    }
    this.feedbackContent = this.feedbackForm.value.feedbackContent.trim();
    return this.feedbackContent === '' || this.feedbackContent.length < MIN_LENGTH;
  }

  checkFeedbackFormAvailability() {
    this._feedbackService.checkFeedbackAvailability(this.currentEventId)
      .then(result => this.formDisabled = !result.feedbackAvailable);
  }
}
