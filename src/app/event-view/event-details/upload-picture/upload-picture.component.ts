import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/catch';
import {UploadPictureService} from './upload-picture.service';
import {Event} from '../../../entities/event';
import {Router} from '@angular/router';

@Component({
  selector: 'app-upload-picture',
  templateUrl: './upload-picture.component.html',
  styleUrls: ['./upload-picture.component.css']
})
export class UploadPictureComponent implements OnInit {

  @Input() showUploading;
  showError = false;
  @Input() event: Event;
  @Output() pictureChanged: EventEmitter<any> = new EventEmitter();

  constructor(private upload: UploadPictureService,
              private router: Router) {
  }

  ngOnInit() {
  }

  uploadPicture(fileInput: any) {
    this.showUploading = true;
    const fileList: FileList = fileInput.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append(this.event.id, file, file.name);
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      const options = new RequestOptions({headers: headers});

      this.upload.uploadPicture(formData, options)
        .then(res => {
          this.showError = false;
          this.pictureChanged.emit(true);
        })
        .catch(error => {
          this.showError = true;
          if (error.status === 403 && localStorage.getItem('id') !== null) {
            window.location.reload();
          }
        });
    }
  }
}
