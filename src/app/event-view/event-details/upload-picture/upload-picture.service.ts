import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';

@Injectable()
export class UploadPictureService {

  private apiEndPoint = '/api/upload-picture';

  constructor(private http: Http) {
  }

  uploadPicture(formdata: FormData, options: RequestOptions): Promise<any> {
    return this.http.post(`${this.apiEndPoint}`, formdata, options)
      .toPromise();
  }

}
