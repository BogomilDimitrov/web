import {TestBed} from '@angular/core/testing';

import {UploadPictureService} from './upload-picture.service';

describe('UploadPictureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadPictureService]
    });
  });

});
