import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UploadPictureComponent} from './upload-picture.component';

describe('UploadPictureComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploadPictureComponent]
    })
      .compileComponents();
  }));
});
