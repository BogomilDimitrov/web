import {FeedbackUpdatedService} from '../../../shared/shared.service';
import {User} from '../../../entities/user';
import {Event} from '../../../entities/event';
import {EventService} from '../../../dashboard/events/event.service';
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-status-menu',
  templateUrl: './status-menu.component.html',
  styleUrls: ['./status-menu.component.css']
})
export class StatusMenuComponent implements OnInit {

  statuses: String[];
  @Input() canEditStatus: boolean;
  @Input() eventId: string;
  @Input() currentEvent: Event;
  currentUser: User;

  constructor(private _service: EventService, private _sharedService: FeedbackUpdatedService) {
    this.currentEvent = new Event();
    this.currentUser = new User();
  }

  ngOnInit() {
    this.currentEvent.status = '';
    this.getAllStatuses();
  }

  changeStatus(type: string) {
    const params = {
      'eventId': this.eventId,
      'statusType': type
    };
    this._service.changeEventStatus(params)
      .then(() => {
        this.currentEvent.status = type;
        this._sharedService.sendClosedEventUpdate('Event closed');
      });
  }

  getAllStatuses() {
    this._service.getAllStatuses()
      .then((status) => {
        this.statuses = status;
      });
  }
}
