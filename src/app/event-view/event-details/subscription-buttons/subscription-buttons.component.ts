import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EventService} from '../../../dashboard/events/event.service';

@Component({
  selector: 'app-subscription-buttons',
  templateUrl: './subscription-buttons.component.html',
  styleUrls: ['./subscription-buttons.component.css']
})
export class SubscriptionButtonsComponent implements OnInit {
  currentSubscription: String;
  subscriptionTypes: String[];
  @Input() eventId: string;
  @Output() changedAttendeesCount = new EventEmitter();

  constructor(private _service: EventService) {
  }

  ngOnInit() {
    this.getCurrentSubscription();
    this.getAllSubscriptionTypes();
  }

  getCurrentSubscription() {
    this._service.getEventSubscriber(this.eventId)
      .then((option) => this.currentSubscription = option);
  }

  getAllSubscriptionTypes() {
    this._service.getAllSubscriptionTypes()
      .then((subscriptions) => this.subscriptionTypes = subscriptions);
  }

  changeSubscription(type: string) {
    const params = {
      'eventId': this.eventId,
      'subscriptionType': type
    };
    this._service.subscribeForEvent(params)
      .then(() => {
        this.currentSubscription = type;
        this.changedAttendeesCount.emit(this.currentSubscription);
      });
  }
}
