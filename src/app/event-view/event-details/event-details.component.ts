import {MessageService} from '../../shared/shared.service';
import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {Event} from '../../entities/event';
import {TimeZoneConverter} from '../../shared/time-zone-converter';
import {User} from '../../entities/user';
import {OidcService} from '../../oidc.service';
import {EventService} from '../../dashboard/events/event.service';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRoute, Router} from '@angular/router';

const USER_ROLE_ADMIN = 'ADMIN';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit, DoCheck {
  @Input() currentEvent: Event;
  @Input() eventRating: number;
  id: string;
  currentUser: User;
  goingAttendeesCount: number;
  interestedAttendeesCount: number;
  pictureUrl: string;
  subscription: Subscription;
  showUploading = false;

  constructor(private _userService: OidcService,
              private _eventService: EventService,
              private _router: Router,
              private _route: ActivatedRoute) {
    this._route.params.subscribe(params => {
      this.id = params['id'];
    });
  }


  ngOnInit(): void {
    this.currentUser = new User();
    this._userService.getCurrentUser().then((user) => {
      this.currentUser.id = user.id;
      this.currentUser.name = user.name;
      this.currentUser.role = user.role;
      this.currentUser.enableNotifications = user.enableNotifications;
      this.currentUser.email = user.email;
      this.currentUser.pictureURL = user.pictureURL;
      this.currentUser.sub = user.sub;
    });
    window.scrollTo(0, 0);

    this.calculateAttendeesCount();
  }

  ngDoCheck(): void {
    this.pictureUrl = this.getPictureName();
  }

  date(): Date {
    return TimeZoneConverter.convertToUTCDate(this.currentEvent.date);
  }

  dueDate(): Date {
    return TimeZoneConverter.convertToUTCDate(this.currentEvent.dueDate);
  }

  canEdit(): boolean {
    if (this.currentUser.id === this.currentEvent.creator.id || this.currentUser.role === USER_ROLE_ADMIN) {
      return true;
    } else {
      return false;
    }
  }

  generateReport(eventId: string) {
    this._eventService.generateReport(eventId);
  }

  calculateAttendeesCount() {
    this._eventService.getEventSubscriptions(this.id)
      .then((subs) => {
        const goingSubscriptions = subs.filter((sub) => sub.subscriptionType === 'GOING');
        this.goingAttendeesCount = goingSubscriptions.length;
        const interestedSubscriptions = subs.filter((sub) => sub.subscriptionType === 'INTERESTED');
        this.interestedAttendeesCount = interestedSubscriptions.length;
      });
  }

  getPictureName(): string {
    return '/assets/images/' + this.currentEvent.id + '.jpg?' + Math.random();
  }

  reloadContent() {
    this.showUploading = true;
    this.currentEvent.havingPicture = false;
    setTimeout(() => {
      this.currentEvent.havingPicture = true;
      this.showUploading = false;
    }, 2000);

  }
}
