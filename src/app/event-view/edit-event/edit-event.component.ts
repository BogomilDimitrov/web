import {Component, Input, OnInit} from '@angular/core';
import {MdDialog} from '@angular/material';
import {Event} from '../../entities/event';
import {EditEventDialogComponent} from './edit-event-dialog/edit-event-dialog.component';
import {TimeZoneConverter} from '../../shared/time-zone-converter';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {

  @Input() event: Event;
  @Input() eventId: string;
  currentDate = new Date();

  constructor(public dialog: MdDialog) {
    this.event = new Event();
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(EditEventDialogComponent, {
      width: '50%',
      data: {
        event: this.event,
        eventId: this.eventId
      }
    });

    dialogRef.afterClosed().subscribe((result: Event) => {
      if (result) {
        this.event = result;
      }
    });
  }

  disableEditButton(): boolean {
    const eventDate = TimeZoneConverter.convertToUTCDate(this.event.date);
    return this.event.status === 'CLOSED' || this.event.status === 'CANCELLED'
      || eventDate < this.currentDate;
  }
}
