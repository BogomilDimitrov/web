import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Group} from '../../../entities/group';
import {Event} from '../../../entities/event';
import {EventService} from '../../../dashboard/events/event.service';
import {MD_DIALOG_DATA, MdDatepickerInputEvent, MdDialogRef} from '@angular/material';
import {TIME_REGEX, URL_REGEX} from '../../../create-new-event/new-event-dialog/new-event-dialog.component';
import {TimeZoneConverter} from '../../../shared/time-zone-converter';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-event-dialog',
  templateUrl: './edit-event-dialog.component.html',
  styleUrls: ['./edit-event-dialog.component.css']
})
export class EditEventDialogComponent implements OnInit {
  placeholder = 'Group';
  editEventForm: FormGroup;
  eventDate: Date;
  registrationEndDate: Date;
  minRegistrationEndDate = new Date();
  minNameLength = 3;
  minLocationLength = 2;
  minEventDate = new Date();

  chosenGroupNames = [];
  allGroupNames = [];
  allGroups: Group[] = [];
  chosenGroups: Group[] = [];

  editEvent: Event;

  constructor(private eventService: EventService,
              private _router: Router,
              public dialogRef: MdDialogRef<EditEventDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
    this.editEvent = data.event;
    this.editEvent.id = data.eventId;

  }

  ngOnInit() {
    this.editEvent.assignedGroups.forEach((group) => this.chosenGroupNames.push({display: group.name, value: group.name}));

    this.eventDate = TimeZoneConverter.convertToUTCDate(this.editEvent.date);
    this.registrationEndDate = TimeZoneConverter.convertToUTCDate(this.editEvent.dueDate);
    this.minEventDate = this.registrationEndDate;

    this.eventService.getAllGroups().then((groups) => {
        this.allGroups = groups;
        this.allGroups.forEach((group) => {
            this.allGroupNames.push({display: group.name, value: group.name});
        });
      }
    );

    this
      .editEventForm = new FormGroup({
      name: new FormControl(this.editEvent.name,
        [Validators.required, Validators.minLength(this.minNameLength), Validators.maxLength(150)]),
      location: new FormControl(this.editEvent.location,
        [Validators.required, Validators.minLength(this.minLocationLength), Validators.maxLength(254)]),
      description: new FormControl(this.editEvent.description, Validators.max(5000)),
      link: new FormControl(this.editEvent.link, Validators.pattern(URL_REGEX)),
      eventTime: new FormControl(`${this.eventDate.getHours()}:${this.formatTimeMinutes(this.eventDate.getMinutes())}`,
        [Validators.pattern(TIME_REGEX), Validators.required]),
      endTime: new FormControl(`${this.registrationEndDate.getHours()}:${this.formatTimeMinutes(this.registrationEndDate.getMinutes())}`,
        [Validators.pattern(TIME_REGEX), Validators.required]),
      groups: new FormControl('')
    });
  }

  onSubmit() {
    const currentEvent = this.editEventFromObject(this.editEventForm.value);
    this.dialogRef.close(currentEvent);
    this.eventService.editEvent(currentEvent).catch((error) => {
      if (error.status >= 500 && error.status <= 511) {
        this._router.navigateByUrl('internal-error');
      }
    });
  }

  onCancel(): void {
    this.dialogRef.close(this.editEvent);
  }

  editEventFromObject(form: any): Event {
    this.editEvent.name = form.name;
    this.editEvent.location = form.location;
    const eventTimeArr: number[] = this.splitTime(form.eventTime);
    this.eventDate.setHours(eventTimeArr[0], eventTimeArr[1]);
    this.editEvent.date = TimeZoneConverter.convertToTimestamp(this.eventDate);
    const endTimeArr: number[] = this.splitTime(form.endTime);
    this.registrationEndDate.setHours(endTimeArr[0], endTimeArr[1]);
    this.editEvent.dueDate = TimeZoneConverter.convertToTimestamp(this.registrationEndDate);
    this.editEvent.description = form.description;
    this.editEvent.link = form.link;

    this.chosenGroupNames.forEach((groupName) => this.chosenGroups.push(new Group(groupName.display)));
    this.editEvent.assignedGroups = this.chosenGroups;
    return this.editEvent;
  }

  setEventDate(currentDate: MdDatepickerInputEvent<Date>): void {
    this.eventDate = currentDate.value;
  }

  setRegistrationEndDate(currentDate: MdDatepickerInputEvent<Date>): void {
    this.registrationEndDate = currentDate.value;
    this.minEventDate = this.registrationEndDate;
  }

  splitTime(time: string): number[] {
    const timeArr: number[] = [];
    time.split(':').forEach((x) => timeArr.push(Number(x)));
    return timeArr;
  }

  private formatTimeMinutes(minutes: number): string {
    if (minutes <= 9) {
      return `0${minutes}`;
    } else {
      return `${minutes}`;
    }
  }
}
