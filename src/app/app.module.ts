import {MessageService, FeedbackUpdatedService, CreateEventUpdateService} from './shared/shared.service';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {LoginComponent} from './login.component';
import {OidcService} from './oidc.service';
import {AppRoutingModule} from './router.config';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {EventViewModule} from './event-view/event-view.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {OAuthModule} from 'angular-oauth2-oidc';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './auth-guard';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {NotificationsComponent} from './user-settings/notifications/notifications.component';
import {SettingsNavComponent} from './user-settings/settings-nav/settings-nav.component';
import {PersonalDataComponent} from './user-settings/personal-data/personal-data.component';
import {NotificationDataService} from './user-settings/notifications/notification-data.service';
import {PersonalDataService} from './user-settings/personal-data/personal-data.service';
import {CreateNewEventModule} from './create-new-event/create-new-event.module';
import {GroupSettingsComponent} from './user-settings/group-settings/group-settings.component';
import {GroupSettingsDataService} from './user-settings/group-settings/group-settings-data.service';
import {CapitalizePipe} from './user-settings/notifications/capitalize-pipe';
import {AdminPanelModule} from './admin-panel/admin-panel.module';
import {RegisterUserComponent} from './register-user/register-user.component';
import {RegisterLoginService} from './register-login.service';
import {SignInComponent} from './sign-in/sign-in.component';
import { Dir } from '@angular/material';
import {InternalErrorComponent} from './internal-error/internal-error.component';
import {BadRequestPageComponent} from './bad-request-page/bad-request-page.component';
import {NotFoundPageComponent} from './not-found-page/not-found-page.component';
import {EventCalendarModule} from './event-calendar/event-calendar.module';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { FooterComponent } from './footer/footer.component';
import { AcceptTermsAndConditionsComponent } from './accept-terms-and-conditions/accept-terms-and-conditions.component';

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    MaterialModule,
    DashboardModule,
    EventViewModule,
    CreateNewEventModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AdminPanelModule,
    OAuthModule.forRoot(),
    AdminPanelModule,
    EventCalendarModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavBarComponent,
    UserSettingsComponent,
    NotificationsComponent,
    SettingsNavComponent,
    PersonalDataComponent,
    GroupSettingsComponent,
    CapitalizePipe,
    RegisterUserComponent,
    SignInComponent,
    InternalErrorComponent,
    BadRequestPageComponent,
    NotFoundPageComponent,
    TermsAndConditionsComponent,
    FooterComponent,
    AcceptTermsAndConditionsComponent
  ],
  providers: [
    OidcService,
    AuthGuard,
    NotificationDataService,
    PersonalDataService,
    GroupSettingsDataService,
    RegisterLoginService,
    LoginComponent,
    Dir,
    MessageService,
    FeedbackUpdatedService,
    CreateEventUpdateService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    RegisterUserComponent,
    SignInComponent
  ]
})
export class AppModule {
}
