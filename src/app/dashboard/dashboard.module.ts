import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../material/material.module';
import {EventComponent} from './events/event/event.component';
import {EventService} from './events/event.service';
import {DashboardComponent} from './dashboard.component';
import {EventsComponent} from './events/events.component';
import {AppRoutingModule} from '../router.config';
import {SearchComponent} from './search/search.component';
import {Ng2FilterPipeModule} from 'ng2-filter-pipe';
import {CompareCreatorPipe} from './search/pipes/compareCreator.pipe';
import {CompareDateAfterPipe} from './search/pipes/compareDateAfter.pipe';
import {CompareDateBeforePipe} from './search/pipes/compareDateBefore.pipe';
import {MyEventsPipe} from './search/pipes/myEvents.pipe';
import {CapitalizePipe} from './search/pipes/capitalize.pipe';
import {RemoveUnderscorePipe} from './search/pipes/remove-underscore.pipe';
import {OidcService} from '../oidc.service';

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    Ng2FilterPipeModule
  ],
  exports: [
    EventComponent,
    EventsComponent,
    DashboardComponent,
    Ng2FilterPipeModule
  ],
  declarations: [
    EventComponent,
    EventsComponent,
    DashboardComponent,
    SearchComponent,
    CompareCreatorPipe,
    CompareDateAfterPipe,
    CompareDateBeforePipe,
    MyEventsPipe,
    CapitalizePipe,
    RemoveUnderscorePipe
  ],
  providers: [
    EventService,
    OidcService,
    MyEventsPipe
  ],
  bootstrap: [
    DashboardComponent
  ]
})
export class DashboardModule {
}

