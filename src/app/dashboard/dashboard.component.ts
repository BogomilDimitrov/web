import {Component} from '@angular/core';
import {EventService} from './events/event.service';
import {Event} from '../entities/event';
import {OidcService} from '../oidc.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  areShown = true;
  events: Event[] = [];

  constructor(private _eventService: EventService,
              private _oidcService: OidcService) {
    this.loadEvents();
  }

  checkForSearchInput(searchCriteriaPresent: any[]): void {
    this.areShown = !(searchCriteriaPresent[0]
      || searchCriteriaPresent[1]
      || searchCriteriaPresent[2]);
  }
  

  private loadEvents() {
    this._eventService.getEvents()
      .then((events) => {
        this.events = events;
        this.events.sort(function (a, b) {
          return (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0);
        });
      })
      .catch((error) => {
        if (error.status === 403 && localStorage.getItem('id') !== null) {
          this._oidcService.isStillLogin().then((resp) => this.loadEvents());
        }
      });
  }

}
