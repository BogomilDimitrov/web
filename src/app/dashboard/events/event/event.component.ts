import {Component, Input, OnInit} from '@angular/core';
import {Event} from '../../../entities/event';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  @Input() event: Event;
  isStillLogin = localStorage.getItem('Login');

  constructor() {
    this.event = new Event();
  }

  ngOnInit() {
    const date = this.event.date;
    const newDate = new Date(date);
    const tzDifference = newDate.getTimezoneOffset();
    const offsetTime = new Date(newDate.getTime() - tzDifference * 60 * 1000);
    this.event.date = offsetTime.getTime();
  }

  getPictureName(): string {
    return '/assets/images/' + this.event.id + '.jpg';
  }
}
