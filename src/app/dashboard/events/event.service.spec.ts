import {async, inject, TestBed} from '@angular/core/testing';
import {HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend} from '@angular/http';
import {EventService} from './event.service';
import {Event} from '../../entities/event';
import {Comment} from '../../entities/comment';
import {User} from '../../entities/user';
import {MockBackend} from '@angular/http/testing';
import {EventSubscriber} from '../../entities/event-subscriber';

const TEST_USER: User = {
  id: 'userTest',
  name: 'test',
  username: 'test',
  password: 'password',
  email: 'test@gmail.com',
  role: 'USER',
  sub: '3243',
  enableNotifications: true,
  pictureURL: 'test'
};

const TEST_EVENT: Event = {
  name: 'TestEventName',
  id: 'thisIsId',
  date: 123,
  description: 'test',
  link: 'test',
  location: '',
  status: 'test',
  dueDate: 123,
  creator: TEST_USER,
  dateOfCreation: 465,
  assignedGroups: [],
  subscriptionType: 'GOING',
  havingPicture: false
};

const TEST_COMMENT: Comment = {
  id: 'commentTest',
  content: 'Great event!',
  dateOfCreation: 123,
  creator: TEST_USER,
  event: TEST_EVENT
};

const TEST_SUBSCRIPTION: EventSubscriber = {
  id: 'TestSubscription123',
  subscriptionType: 'GOING',
  feedbackComment: 'Great Event',
  feedbackRating: 5,
  subscriber: TEST_USER
};

describe('EventServices', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        EventService,
        {provide: XHRBackend, useClass: MockBackend}
      ]
    });
  });

  it('should be created', inject([EventService], (service: EventService) => {
    expect(service).toBeTruthy();
  }));

  describe('getEvents()', () => {
    it('should contain 1 event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_EVENT]
          })));
        });
        eventService.getEvents().then((events: Event[]) => {
          expect(lastConnection.request.url).toMatch(/.*\/api\/events\/all/g, 'url not valid');
          expect(lastConnection.request.withCredentials).toEqual(true);
          expect(events.length).toEqual(1, 'should contain given amount of events');
          expect(events[0]).toEqual(TEST_EVENT, ' FIRST_EVENT should be the first event');
        });
      })));
  });

  describe('getMyEvents()', () => {
    it('should contain 1 event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_EVENT]
          })));
        });
        eventService.getMyEvents().then((events: Event[]) => {
          expect(lastConnection.request.url).toMatch(/.*\/api\/events\/myEvents/g, 'url not valid');
          expect(lastConnection.request.withCredentials).toEqual(true);
          expect(events.length).toEqual(1, 'should contain given amount of events');
          expect(events[0]).toEqual(TEST_EVENT, ' FIRST_EVENT should be the first event');
        });
      })));
  });

  describe('getEventsFromMyGroups()', () => {
    it('should contain 1 event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_EVENT]
          })));
        });
        eventService.getEventsFromMyGroups().then((events: Event[]) => {
          expect(lastConnection.request.url).toMatch(/.*\/api\/user-groups\/inMyGroups/g, 'url not valid');
          expect(lastConnection.request.withCredentials).toEqual(true);
          expect(events.length).toEqual(1, 'should contain given amount of events');
          expect(events[0]).toEqual(TEST_EVENT, ' FIRST_EVENT should be the first event');
        });
      })));
  });


  describe('saveEvent()', () => {
    it('should create new event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            status: 200
          })));

          eventService.saveEvent(TEST_EVENT).then((response) => {
            expect(lastConnection.request.url).toMatch(/.*\/api\/events/g, 'url not valid');
            expect(lastConnection.request.withCredentials).toEqual(true);
            expect(response).toEqual(TEST_EVENT);
          });
        });
      })));
  });

  describe('editEvent()', () => {
    it('should edit existing event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_EVENT]
          })));

          eventService.saveEvent(TEST_EVENT).then((response) => {
            expect(lastConnection.request.url).toMatch(/.*\/api\/events\/edit/g, 'url not valid');
            expect(lastConnection.request.withCredentials).toEqual(true);
            expect(response).toEqual(200);
          });
        });
      })));
  });

  describe('getEventById()', () => {
    it('shoud return event', async(inject([EventService, XHRBackend], (eventService, mockBackend) => {

      const mockResponse = [{
        id: '2323323',
        name: 'HackConf',
        date: '10/10/2018',
        status: 'OPEN'
      }
      ];
      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });
      eventService.getEventById('2323323').then((result) => {
        expect(result[0]).toBeTruthy();
        expect(result[0].name).toBeTruthy();
        expect(result[0].name).toBe('HackConf');
      });

    })));
  });

  describe('getEventAverageRating()', () => {
    it('shoud return rating', async(inject([EventService, XHRBackend], (eventService, mockBackend) => {

      const mockResponse = {
        rating: '3.5'
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });
      eventService.getEventAverageRating(TEST_EVENT.id).then((result) => {
        expect(result).toBeTruthy();
        expect(result.rating).toBeTruthy();
        expect(result.rating).toBe('3.5');
      });
    })));
  });

  describe('subscribeForEvent()', () => {
    it('should subscribe for event', async(inject([EventService, XHRBackend], (eventService, mockBackend) => {
      let lastConnection;
      mockBackend.connections.subscribe((connection) => {
        lastConnection = connection;
        connection.mockRespond(new Response(new ResponseOptions({
          status: 200
        })));
      });
      eventService.subscribeForEvent(TEST_EVENT.id).then((result) => {
        expect(lastConnection.request.withCredentials).toEqual(true);
        expect(result.status).toBe(200);
      });
    })));
  });


  describe('getAllComments()', () => {
    it('should contain given comment', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        mockBackend.connections.subscribe((connection) => {
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_COMMENT]
          })));
        });
        eventService.getAllComments(TEST_EVENT.id).then((comments: Comment[]) => {
          expect(comments.length).toEqual(1);
          expect(comments[0]).toEqual(TEST_COMMENT);
        });
      })));
  });

  describe('postAComment()', () => {
    it('adding comment to given event should return Promise<Comment>', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        const mockResponse = {
          data: [
            {response: TEST_COMMENT}
          ]
        };
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(mockResponse)
          })));
        });

        eventService.postAComment(TEST_COMMENT.content, TEST_EVENT.id).then((data) => {
          expect(data[0].response).toEqual(TEST_COMMENT);
          expect(lastConnection.request.url).toMatch(/.*\api\/comments/g);
          expect(lastConnection.request.method).toEqual(RequestMethod.Post);
        });
      })));
  });

  describe('changeEventStatus()', () => {
    it('should change status for event', async(inject([EventService, XHRBackend], (eventService, mockBackend) => {
      let lastConnection;
      mockBackend.connections.subscribe((connection) => {
        lastConnection = connection;
        connection.mockRespond(new Response(new ResponseOptions({
          status: 200
        })));
      });
      eventService.changeEventStatus(TEST_EVENT.id, 'OPEN').then((result) => {
        expect(lastConnection.request.withCredentials).toEqual(true);
        expect(result.status).toBe(200);
      });
    })));
  });

  describe('getEventSubscriptions()', () => {
    it('should contain 1 subscription', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_SUBSCRIPTION]
          })));
        });
        eventService.getEventSubscriptions('123TestEvent123').then((subscriptions: EventSubscriber[]) => {
          expect(lastConnection.request.withCredentials).toEqual(true);
          expect(subscriptions.length).toEqual(1, 'should contain given amount of subscriptions');
          expect(subscriptions[0]).toEqual(TEST_SUBSCRIPTION, ' TEST_SUBSCRIPTION should be the subscription');
        });
      })));
  });

  describe('getEventsWithLimit()', () => {
    it('should contain 1 event', async(inject([EventService, XHRBackend],
      (eventService, mockBackend) => {
        let lastConnection;
        mockBackend.connections.subscribe((connection) => {
          lastConnection = connection;
          connection.mockRespond(new Response(new ResponseOptions({
            body: [TEST_EVENT]
          })));
        });
        eventService.getEventsWithLimit(1).then((events: Event[]) => {
          expect(lastConnection.request.url).toMatch('api\/events\/limit\/1', 'url not valid');
          expect(lastConnection.request.withCredentials).toEqual(true);
          expect(events.length).toEqual(1, 'should contain given amount of events');
          expect(events[0]).toEqual(TEST_EVENT, ' FIRST_EVENT should be the first event');
        });
      })));
  });
});
