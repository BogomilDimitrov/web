import {Injectable} from '@angular/core';
import {Headers, Http, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {Event} from '../../entities/event';
import {Group} from '../../entities/group';
import {Comment} from '../../entities/comment';
import 'rxjs/Rx';
import {EventSubscriber} from '../../entities/event-subscriber';
import {Router} from '@angular/router';

@Injectable()
export class EventService {
  private _baseUrl = '/api/events';
  private _commentUrl = '/api/comments';
  private _groupsUrl = '/api/user-groups';
  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private _http: Http, private router: Router) {
  }

  getEvents(): Promise<Event[]> {
    return this._http.get(`${this._baseUrl}/all`, {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }

  getEventById(eventId: string): Promise<Event> {
    return this._http.get(`${this._baseUrl}/${eventId}`, {
      withCredentials: true
    })
      .map(response => response.json() as Event)
      .toPromise()
      .catch(this.handleError);
  }

  saveEvent(event: Event): Promise<Event> {
    return this._http.post(this._baseUrl, event, {
      withCredentials: true
    })
      .map(response => response.json() as Event)
      .toPromise()
      .catch(this.handleError);
  }

  generateReport(eventId: string) {
    window.open(`api/reports/${eventId}`, '_blank');
  }


  editEvent(event: Event): Promise<Event> {
    return this._http.patch('api/events/edit', event, {
      withCredentials: true
    })
      .toPromise()
      .then((response) => response.json() as Event)
      .catch(this.handleError);
  }

  getEventAverageRating(eventId: string): Promise<number> {
    return this._http.get(`api/feedback/eventRating/${eventId}`, {
      withCredentials: true
    })
      .map(response => response.json() as number)
      .toPromise();
  }

  getEventSubscriber(eventId: string): Promise<string> {
    return this._http.get(`api/subscribe/userSubscribeType/${eventId}`, {
      withCredentials: true
    })
      .map(response => response.json() as any)
      .toPromise()
      .catch(this.handleError);
  }

  subscribeForEvent(params): Promise<string> {
    const urlSearchParams = new URLSearchParams();
    urlSearchParams.append('eventId', params.eventId);
    urlSearchParams.append('subscriptionType', params.subscriptionType);

    const body = urlSearchParams.toString();
    return this._http.patch('api/subscribe', body, {
      headers: this.headers,
      withCredentials: true
    })
      .toPromise()
      .catch(this.handleError);
  }

  getAllGroups(): Promise<Group[]> {
    return this._http.get('api/groups', {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as Group[])
      .catch(this.handleError);
  }

  postAComment(commentContent: string, eventId: string): Promise<Comment> {
    const timeOfPosting = Date.now();
    const timeStamp = new Date(timeOfPosting);
    const body = new URLSearchParams();
    body.set('commentContent', commentContent);
    body.set('timeOfPosting', timeStamp.toISOString().replace('T', ' ').replace('Z', ''));
    body.set('eventId', eventId);
    return this._http.post(this._commentUrl, body, {
      withCredentials: true
    })
      .toPromise()
      .then(res => res.json().data as Comment)
      .catch(this.handleError);
  }

  getAllComments(eventId: string): Promise<Comment[]> {
    return this._http.get(`${this._commentUrl}/${eventId}`, {
      withCredentials: true
    })
      .toPromise()
      .then((result) => result.json() as Comment[])
      .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }

  getMyEvents(): Promise<Event[]> {
    return this._http.get(`${this._baseUrl}/myEvents`, {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }

  getEventsFromMyGroups(): Promise<Event[]> {
    return this._http.get(`${this._groupsUrl}/inMyGroups`, {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }
  

  getOtherEvents(): Promise<Event[]> {
    return this._http.get(`${this._baseUrl}`, {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }

  changeEventStatus(params): Promise<Response> {
    return this._http.patch('api/events/changeStatus', params, {
      withCredentials: true
    }).toPromise()
      .catch(this.handleError);
  }

  getEventsWithLimit(limit: number): Promise<Event[]> {
    return this._http.get(`api/events/limit/${limit}`, {
      withCredentials: true
    })
      .toPromise()
      .then((result) => result.json() as Event[])
      .catch(this.handleError);
  }

  getEventSubscriptions(eventId: String): Promise<EventSubscriber[]> {
    return this._http.get(`api/subscribe/${eventId}`, {
      withCredentials: true
    }).toPromise()
      .then(response => response.json() as EventSubscriber[])
      .catch(this.handleError);
  }

  getAllStatuses(): Promise<String[]> {
    return this._http.get('api/events/getAllStatuses', {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as String[])
      .catch(this.handleError);
  }

  getAllSubscriptionTypes(): Promise<String[]> {
    return this._http.get('api/subscribe/getAllSubscriptionTypes', {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as String[])
      .catch(this.handleError);
  }
}
