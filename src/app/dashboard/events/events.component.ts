import {Component, OnInit} from '@angular/core';
import {Event} from '../../entities/event';
import {EventService} from './event.service';
import {SectionExpansion} from '../../shared/section-expansion';
import {EventSectionExpansion} from '../../shared/events-section-expansion';
import {OidcService} from '../../oidc.service';
import {MyEventsPipe} from '../search/pipes/myEvents.pipe';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent implements OnInit {

  myEvents: Event[] = [];
  eventsInMyGroups: Event[] = [];
  otherEvents: Event[] = [];
  sectionOneLoaded = false;
  sectionTwoLoaded = false;
  sectionThreeLoaded = false;
  id: string;

  myEventsSectionExpansion: SectionExpansion;
  inMyGroupsSectionExpansion: SectionExpansion;
  otherEventsSectionExpansion: SectionExpansion;

  constructor(private _eventService: EventService,
              private _oidcService: OidcService,
              private _myEventsPipe: MyEventsPipe) {
  }

  ngOnInit(): void {
    this.id = localStorage.getItem('id');
    this.loadEventsInMyGroups();
    this.loadMyEvents();
    this.loadOtherEvents();
    if (localStorage.getItem('Login') && !sessionStorage.getItem('currentSession')) {
      this._oidcService.isStillLogin().then((resp) => {
        sessionStorage.setItem('currentSession', 'true');
      });
    }
  }

  private loadOtherEvents() {
    this.otherEventsSectionExpansion = new SectionExpansion('More');
    this._eventService.getOtherEvents()
      .then((events) => {
        this.otherEvents = events;
        this.otherEvents.sort(function (a, b) {
          return ( a.date > b.date ) ? 1 : ( ( b.date > a.date ) ? -1 : 0 );
        });
        this.otherEventsSectionExpansion = EventSectionExpansion.loadEvents(this.otherEvents);
        this.sectionThreeLoaded = true;
      }).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this._oidcService.isStillLogin().then((resp) => window.location.reload());
      }
    });
  }

  private loadMyEvents() {
    this.myEventsSectionExpansion = new SectionExpansion('More');
    this._eventService.getMyEvents()
      .then((events) => {
        this.myEvents = events;
        this.myEvents.sort(function (a, b) {
          return ( a.date > b.date ) ? 1 : ( ( b.date > a.date ) ? -1 : 0 );
        });
        this.myEventsSectionExpansion = EventSectionExpansion.loadEvents(this.myEvents);
        this.sectionOneLoaded = true;
      }).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this._oidcService.isStillLogin().then((resp) => this.loadMyEvents());
      }
    });
  }

  private loadEventsInMyGroups() {
    this.inMyGroupsSectionExpansion = new SectionExpansion('More');
    this._eventService.getEventsFromMyGroups()
      .then((events) => {
        this.eventsInMyGroups = events;
        this.eventsInMyGroups.sort(function (a, b) {
          return ( a.date > b.date ) ? 1 : ( ( b.date > a.date ) ? -1 : 0 );
        });
        this.inMyGroupsSectionExpansion = EventSectionExpansion.loadEvents(this.eventsInMyGroups);
        this.sectionTwoLoaded = true;
      }).catch((error) => {
      if (error.status === 403 && localStorage.getItem('id') !== null) {
        this._oidcService.isStillLogin().then((resp) => {
          sessionStorage.setItem('currentSession', 'true');
          this.loadEventsInMyGroups();
        });
      }
    });
  }

}
