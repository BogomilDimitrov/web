import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'myEvents',
})

export class MyEventsPipe implements PipeTransform {
  transform(events: any, id: string): any {
    if (id !== '') {
      return events.filter(e => e.creator.id === id
        || e.subscriptionType === 'GOING'
        || e.subscriptionType === 'INTERESTED');
    } else {
      return [];
    }
  }
}
