import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'compareCreator',
})

export class CompareCreatorPipe implements PipeTransform {
  transform(events: any, creatorName: any): any {
    creatorName = creatorName.toLowerCase().trim();
    if (creatorName !== '') {
      return events.filter(e => e.creator.name.toLowerCase().includes(creatorName));
    } else {
      return events;
    }
  }
}
