import {Pipe, PipeTransform} from '@angular/core';
import {TimeZoneConverter} from '../../../shared/time-zone-converter';

@Pipe({
  name: 'compareDateBefore',
})
export class CompareDateBeforePipe implements PipeTransform {
  date: Date;

  transform(events: any, dateBefore: any): any {
    if (dateBefore !== null && dateBefore !== this.date) {
      dateBefore.setHours(23, 59, 59);
      const timeZoneDate = new Date(dateBefore);
      dateBefore = TimeZoneConverter.convertToTimestamp(timeZoneDate);
      return events.filter(e => e.date <= ( new Date(dateBefore) ).getTime());
    } else {
      return events;
    }
  }
}
