import {Pipe, PipeTransform} from '@angular/core';
import {TimeZoneConverter} from '../../../shared/time-zone-converter';

@Pipe({
  name: 'compareDateAfter',
})
export class CompareDateAfterPipe implements PipeTransform {
  date: Date;

  transform(events: any, dateAfter: any): any {
    if (dateAfter !== '' && dateAfter !== this.date) {
      const timeZoneDate = new Date(dateAfter);
      dateAfter = TimeZoneConverter.convertToTimestamp(timeZoneDate);
      return events.filter(e => e.date >= ( new Date(dateAfter) ).getTime());
    } else {
      return events;
    }
  }
}
