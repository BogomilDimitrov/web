import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {EventService} from '../events/event.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Event} from '../../entities/event';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {OidcService} from '../../oidc.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})

export class SearchComponent implements OnInit, OnChanges {
  newEventForm: FormGroup;
  statuses: String[];
  eventFilter = {name: '', status: {$or: []}};
  eventFilterOld = {status: {$or: ['CLOSED', 'CLOSED_FOR_ENROLLMENT']}};
  creatorName = '';
  anyInput = false;
  dateBefore = null;
  dateAfter = null;
  checkedOld = false;
  checkedMyEvents = false;
  id: string;
  hideToggle = false;
  disabled = true;
  @Output() hideEvents = new EventEmitter<any[]>();
  @Input() events: Event[];
  creatorNames: Event[];
  creatorNamesInit: Event[];
  eventCtrl: FormControl = new FormControl();
  creatorCtrl: FormControl = new FormControl();
  filteredEventsByName: Observable<any[]>;
  filteredEventsByCreator: Observable<any[]>;

  constructor(private _eventService: EventService,
              private _userService: OidcService) {
  }

  ngOnInit(): void {
    this.id = localStorage.getItem('id');
    this._eventService.getAllStatuses().then((statuses) => {
      this.statuses = statuses;
      this.eventFilter.status.$or = this.statuses;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.filteredEventsByName =
      this.eventCtrl
        .valueChanges
        .startWith(null)
        .map(event => event ? this.filterEvents(event) : changes.events.currentValue.slice(0, 5));
    this.filteredEventsByCreator =
      this.creatorCtrl
        .valueChanges
        .startWith(null)
        .map(event => this.filterEventsCreator(event));
    this.newEventForm = new FormGroup({});
  }

  showOld() {
    if (this.checkedOld) {
      this.checkedOld = !this.checkedOld;
    } else {
      this.checkedMyEvents = false;
      this.checkedOld = !this.checkedOld;
    }
    this.checkForInput();
  }

  showMyEvents() {
    if (this.checkedMyEvents) {
      this.checkedMyEvents = !this.checkedMyEvents;
    } else {
      this.checkedOld = false;
      this.checkedMyEvents = !this.checkedMyEvents;
    }
    this.checkForInput();
  }

  checkForInput() {
    this.anyInput = !( ( this.eventFilter.status.$or.length === 0
      || this.eventFilter.status.$or.length === 4 )
      && this.eventFilter.name === ''
      && ( this.dateBefore === '' || this.dateBefore === null )
      && ( this.dateAfter === '' || this.dateAfter === null )
      && this.creatorName === '' );
    this.hideEvents.emit([this.anyInput, this.checkedOld, this.checkedMyEvents]);
  }

  filterEvents(name: string) {
    return this.events.filter(event =>
      event.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
      .slice(0, 5);
  }

  filterEventsCreator(name: string) {
    if (name !== null) {
      this.creatorNames = this.events.filter(event =>
        event.creator.name.toLowerCase().indexOf(name.toLowerCase()) === 0).slice(0, 5);
      for (let i = 0; i < this.creatorNames.length; i++) {
        for (let j = i + 1; j < this.creatorNames.length; j++) {
          if ((this.creatorNames[i].creator.name === this.creatorNames[j].creator.name)) {
            this.creatorNames.splice(j, 1);
            j--;
          }
        }
      }
      return this.creatorNames;
    } else {
      this.creatorNamesInit = this.events;
      for (let i = 0; i < this.creatorNamesInit.length; i++) {
        for (let j = i + 1; j < this.creatorNamesInit.length; j++) {
          if (this.creatorNamesInit[i].creator.name === this.creatorNamesInit[j].creator.name) {
            this.creatorNamesInit.splice(j, 1);
            j--;
          }
        }
        return this.creatorNamesInit.slice(0, 5);
      }
    }
  }

  resetForm() {
    this.eventFilter.status =  {$or: this.statuses};
    this.creatorName = '';
    this.dateAfter = null;
    this.dateBefore = null;
    this.checkForInput();
  }
}
