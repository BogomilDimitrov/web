import {NgModule} from '@angular/core';
import {DateFormat} from '../shared/date-format';
import {
  DateAdapter,
  MdAutocompleteModule,
  MdButtonModule,
  MdButtonToggleModule,
  MdCardModule,
  MdCheckboxModule,
  MdChipsModule,
  MdDatepickerModule,
  MdDialogModule,
  MdExpansionModule,
  MdIconModule,
  MdInputModule,
  MdMenuModule,
  MdNativeDateModule,
  MdOptionModule,
  MdRadioModule,
  MdSelectModule,
  MdSidenavModule,
  MdSlideToggleModule,
  MdSnackBarModule,
  MdTableModule,
  MdTabsModule,
  MdToolbarModule,
  MdTooltipModule,
  MatProgressSpinnerModule
} from '@angular/material';
import {NewEventDialogComponent} from '../create-new-event/new-event-dialog/new-event-dialog.component';
import {TagInputModule} from 'ngx-chips';
import 'rxjs/add/operator/debounceTime';

@NgModule({
  imports: [
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdDialogModule,
    MdCardModule,
    MdExpansionModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdChipsModule,
    MdIconModule,
    MdAutocompleteModule,
    TagInputModule,
    MdButtonToggleModule,
    MdToolbarModule,
    MdSidenavModule,
    MdTabsModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdTooltipModule,
    MdSelectModule,
    MdOptionModule,
    MdTableModule,
    MdTooltipModule,
    MdSnackBarModule,
    MdRadioModule,
    MatProgressSpinnerModule],
  exports: [
    MdButtonModule,
    MdCheckboxModule,
    MdInputModule,
    MdCardModule,
    MdExpansionModule,
    MdDialogModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdChipsModule,
    MdIconModule,
    MdAutocompleteModule,
    TagInputModule,
    MdButtonToggleModule,
    MdToolbarModule,
    MdSidenavModule,
    MdTabsModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdTooltipModule,
    MdSelectModule,
    MdOptionModule,
    MdTableModule,
    MdTooltipModule,
    MdSnackBarModule,
    MatProgressSpinnerModule,
    MdRadioModule
  ],
  declarations: [],
  providers: [
    {provide: DateAdapter, useClass: DateFormat}
  ],
  entryComponents: [
    NewEventDialogComponent
  ]
})
export class MaterialModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale('en-in');
  }
}
