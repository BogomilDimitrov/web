import {Dir, MdSnackBar, MdSnackBarConfig, MdSnackBarVerticalPosition} from '@angular/material';

export class CustomSnackbar {

  verticalPosition: MdSnackBarVerticalPosition = 'top';

  constructor(public snackBar: MdSnackBar, private dir: Dir) {
  }

  openSnackBar(message: string, action: string) {
    const config = new MdSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.duration = 4000;
    config.direction = this.dir.value;
    this.snackBar.open(message, action, config);
  }

}
