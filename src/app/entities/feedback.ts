import {EventSubscriber} from './event-subscriber';

export class Feedback {
  feedbacks: EventSubscriber[];
  rating: number;
}
