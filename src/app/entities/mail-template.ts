export class MailTemplate {
  id: string;
  subject: string;
  filePath: string;
  type: string;
}
