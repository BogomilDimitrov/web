import {Group} from './group';
import {User} from './user';

export class Event {
  id: string;
  name: string;
  date: number;
  description: string;
  link: string;
  location: string;
  status: string;
  dueDate: number;
  creator: User;
  dateOfCreation: number;
  subscriptionType: string;
  assignedGroups: Group[];
  havingPicture: boolean;

  constructor() {
    this.date = 0;
    this.dueDate = 0;
    this.assignedGroups = [];
    this.creator = new User();
  }
}
