export class User {
  id: string;
  username: string;
  password: string;
  name: string;
  email: string;
  sub: string;
  pictureURL: string;
  role: string;
  termsAccepted: boolean;
  enableNotifications: boolean;
}
