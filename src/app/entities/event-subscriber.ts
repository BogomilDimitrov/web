import {User} from './user';

export class EventSubscriber {
  id: string;
  subscriptionType: string;
  feedbackComment: string;
  feedbackRating: number;
  subscriber: User;

  constructor() {
    this.subscriber = new User();
  }
}
