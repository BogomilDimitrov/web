import {User} from './user';
import {MailTemplate} from './mail-template';

export class UserMailSubscription {
  id: string;
  user: User;
  mailTemplate: MailTemplate;
  isEnabled: boolean;


  constructor() {
    this.user = new User();
    this.mailTemplate = new MailTemplate();
  }
}
