import {User} from './user';
import {Event} from './event';

export class Comment {
  id: string;
  content: string;
  dateOfCreation: number;
  creator: User;
  event: Event;

  constructor() {
    this.event = new Event();
    this.creator = new User();
    this.dateOfCreation = 0;
  }
}
