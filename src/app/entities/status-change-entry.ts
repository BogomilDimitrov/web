import {User} from './user';

export class StatusChangeEntry {
  id: string;
  event: string;
  status: string;
  changeData: number;
  userId: User;


  constructor() {
    this.changeData = 0;
    this.userId = new User();
  }
}
