import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDatepickerInputEvent, MdDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EventService} from '../../dashboard/events/event.service';
import {Event} from '../../entities/event';
import {Group} from '../../entities/group';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import {TimeZoneConverter} from '../../shared/time-zone-converter';
import {Router} from '@angular/router';
import {isNullOrUndefined} from 'util';

export const URL_REGEX = '(https?:\\/\\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.' +
  '[^\\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\.' +
  '|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\s]{2,})';

export const TIME_REGEX = '^(2[0-3]|[01]?[0-9]):([0-5]{1}[0-9])$';

@Component({
  selector: 'app-new-event-dialog',
  templateUrl: './new-event-dialog.component.html',
  styleUrls: ['./new-event-dialog.component.css']
})
export class NewEventDialogComponent implements OnInit {
  placeholder = 'Groups';
  newEventForm: FormGroup;
  date = new Date();
  registrationEndDate: Date;
  minDate = new Date();
  minNameLength = 3;
  minLocationLength = 2;
  minRegistrationEndDate = new Date();

  chosenGroupNames = [];
  allGroupNames: string[] = [];
  allGroups: Group[] = [];
  chosenGroups: Group[] = [];

  constructor(private _router: Router,
              private eventService: EventService,
              public dialogRef: MdDialogRef<NewEventDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.minDate.setHours(0, 0, 0, 0);
    this.minRegistrationEndDate.setHours(0, 0, 0, 0);
    this.eventService.getAllGroups().then((groups) => {
      this.allGroups = groups;
      this.allGroups.forEach((group) => this.allGroupNames.push(group.name));
    });

    this.newEventForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(this.minNameLength), Validators.maxLength(150)]),
      location: new FormControl('', [Validators.required, Validators.minLength(this.minLocationLength), Validators.maxLength(254)]),
      description: new FormControl('', Validators.max(5000)),
      link: new FormControl('', Validators.pattern(URL_REGEX)),
      eventTime: new FormControl('', [Validators.pattern(TIME_REGEX), Validators.required]),
      endTime: new FormControl('', [Validators.pattern(TIME_REGEX), Validators.required]),
      groups: new FormControl('')
    });
  }

  onSubmit() {
    const currentEvent = this.createEventFromObject(this.newEventForm.value);
    this.dialogRef.close(currentEvent);
    this.eventService.saveEvent(currentEvent).then((event) => {
      this._router.navigateByUrl(`events/${event.id}`);
    })
      .catch((error) => {
        if (error.status >= 500 && error.status <= 511) {
          this._router.navigateByUrl('internal-error');
        }
      });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isEmptyName(): boolean {
    const name = this.newEventForm.value.name;
    if (name == null) {
      return true;
    }
    return isNullOrUndefined(name) || name.trim() === '' || name.length < this.minNameLength;
  }

  isEmptyLocation(): boolean {
    const location = this.newEventForm.value.location;
    if (location == null) {
      return true;
    }
    return isNullOrUndefined(location) || location.trim() === '' || location.length < this.minLocationLength;
  }

  createEventFromObject(form: any): Event {
    const newEvent = new Event();
    if (isNullOrUndefined(form.name) || isNullOrUndefined(form.location)) {
      newEvent.name = '';
      newEvent.location = '';
    } else {
      newEvent.name = form.name.trim();
      newEvent.location = form.location.trim();
    }
    const eventTimeArr: number[] = this.splitTime(form.eventTime);
    this.date.setHours(eventTimeArr[0], eventTimeArr[1]);
    newEvent.date = TimeZoneConverter.convertToTimestamp(this.date);
    const endTimeArr: number[] = this.splitTime(form.endTime);
    this.registrationEndDate.setHours(endTimeArr[0], endTimeArr[1]);
    newEvent.dueDate = TimeZoneConverter.convertToTimestamp(this.registrationEndDate);
    newEvent.description = form.description;
    newEvent.link = form.link;

    this.chosenGroupNames.forEach((groupName) => {
        this.chosenGroups.push(new Group(groupName.display));
      }
    );
    newEvent.assignedGroups = this.chosenGroups;
    console.log(newEvent);
    return newEvent;
  }

  setEventDate(currentDate: MdDatepickerInputEvent<Date>, form: any): void {
    this.date = currentDate.value;
  }

  setRegistrationEndDate(currentDate: MdDatepickerInputEvent<Date>, form: any): void {
    this.registrationEndDate = currentDate.value;
    this.minDate = this.registrationEndDate;
  }

  splitTime(time: string): number[] {
    const timeArr: number[] = [];
    time.split(':').forEach((x) => timeArr.push(Number(x)));
    return timeArr;
  }

}
