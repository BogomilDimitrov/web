import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateNewEventComponent} from './create-new-event.component';
import {NewEventDialogComponent} from './new-event-dialog/new-event-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {EventService} from '../dashboard/events/event.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
    CreateNewEventComponent,
    NewEventDialogComponent
  ],
  providers: [
    EventService
  ],
  exports: [
    CreateNewEventComponent,
    NewEventDialogComponent
  ],
  bootstrap: [
    CreateNewEventComponent
  ]
})
export class CreateNewEventModule {
}
