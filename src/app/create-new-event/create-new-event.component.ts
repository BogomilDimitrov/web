import {Component, OnInit} from '@angular/core';
import {MdDialog} from '@angular/material';
import {NewEventDialogComponent} from './new-event-dialog/new-event-dialog.component';
import {Event} from '../entities/event';

@Component({
  selector: 'app-create-new-event',
  templateUrl: './create-new-event.component.html',
  styleUrls: ['./create-new-event.component.css']
})
export class CreateNewEventComponent implements OnInit {
  event: Event;

  constructor(public dialog: MdDialog) {
    this.event = new Event();
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewEventDialogComponent, {
      width: '50%',
      data: {event: this.event}
    });

    dialogRef.afterClosed().subscribe((result: Event) => {
      this.event = result;
    });
  }
}
