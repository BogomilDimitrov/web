import {Dir} from '@angular/material';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserManagementComponent} from './user-management/user-management.component';
import {GroupManagementComponent} from './group-management/group-management.component';
import {UploadTemplateComponent} from './upload-template/upload-template.component';
import {AdminPanelComponent} from './admin-panel.component';
import {UserService} from './user.service';
import {Ng2FilterPipeModule} from 'ng2-filter-pipe/dist';
import {ChangeRoleComponent} from './user-management/change-role/change-role.component';
import {RouterModule} from '@angular/router';
import {GroupService} from './group-management/group.serivce';
import {AddGroupComponent} from './group-management/add-group/add-group.component';
import {UploadService} from './upload-template/upload.service';
import {TemplateRenamePipe} from '../shared/template-rename.pipe';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2FilterPipeModule,
    RouterModule
  ],
  exports: [
    UserManagementComponent,
    GroupManagementComponent,
    UploadTemplateComponent,
    AdminPanelComponent
  ],
  declarations: [
    UserManagementComponent,
    GroupManagementComponent,
    UploadTemplateComponent,
    AdminPanelComponent,
    ChangeRoleComponent,
    AddGroupComponent,
    TemplateRenamePipe
  ],
  providers: [
    UserService,
    GroupService,
    UploadService,
    Dir
  ],
  bootstrap: [
    AdminPanelComponent
  ],
  entryComponents: []
})
export class AdminPanelModule {
}
