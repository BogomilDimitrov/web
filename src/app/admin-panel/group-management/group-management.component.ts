import {CustomSnackbar} from '../../material/custom-snackbar';
import {Component, OnInit} from '@angular/core';
import {Group} from '../../entities/group';
import {GroupService} from './group.serivce';
import {Dir, MdSnackBar} from '@angular/material';

@Component({
  selector: 'app-group-management',
  templateUrl: './group-management.component.html',
  styleUrls: ['./group-management.component.css']
})
export class GroupManagementComponent implements OnInit {
  groups: Group[];

  constructor(private _groupService: GroupService, public snackBar: MdSnackBar, private dir: Dir) {
  }

  ngOnInit() {
    this._groupService.getAllGroups().then((groups) => {
      this.groups = groups;
    });
  }

  addToGroups(group: Group) {
    this.groups.push(group);
  }

  remove(groupName: string) {
    this._groupService.removeGroup(groupName)
      .then((response) => {
        if (response.name === groupName) {
          const idx = this.groups.indexOf(this.groups.filter((g) => g.name === groupName)[0]);
          this.groups.splice(idx, 1);
          this.openSnackBar('Group ' + response.name + ' was removed.', '');
        } else {
          this.openSnackBar(response, '');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    const cs = new CustomSnackbar(this.snackBar, this.dir);
    cs.openSnackBar(message, action);
  }
}
