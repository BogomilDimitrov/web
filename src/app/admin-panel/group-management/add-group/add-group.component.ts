import {CustomSnackbar} from '../../../material/custom-snackbar';
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GroupService} from '../group.serivce';
import {Group} from '../../../entities/group';
import {Dir, MdSnackBar} from '@angular/material';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css'],
})
export class AddGroupComponent implements OnInit {
  addGroupForm: FormGroup;
  groupName: string;
  inFocus = false;
  minGroupNameLength = 2;
  @Output() newGroupAdded = new EventEmitter<Group>();

  constructor(private _formBuilder: FormBuilder,
              private _groupService: GroupService, public snackBar: MdSnackBar, private dir: Dir) {
    this.initForm();
  }

  initForm() {
    this.addGroupForm = this._formBuilder.group({
      groupName: ['', Validators.compose([Validators.minLength(this.minGroupNameLength)])]
    });
  }

  ngOnInit() {
  }

  addGroup() {
    this.groupName = this.addGroupForm.value.groupName.trim();
    this._groupService.addGroup(this.groupName)
      .then((response) => {
        if (response.name === this.groupName) {
          this.newGroupAdded.emit(response);
          this.openSnackBar('Group ' + response.name + ' was added.', '');
        } else {
          this.openSnackBar(response, '');
        }
      });
    this.addGroupForm.reset();
  }

  openSnackBar(message: string, action: string) {
    const cs = new CustomSnackbar(this.snackBar, this.dir);
    cs.openSnackBar(message, action);
  }

  isEmptyGroupName(): boolean {
    const name = this.addGroupForm.value.groupName;
    if (name == null) {
      return true;
    }
    return isNullOrUndefined(name) || name.trim() === '' || name.length < this.minGroupNameLength;
  }
}
