import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {Group} from '../../entities/group';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class GroupService {
  private _headersUrl = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  private _baseUrl = '/api/groups';

  constructor(private http: Http) {
  }

  getAllGroups(): Promise<Group[]> {
    return this.http
      .get(`/api/groups`, {
        withCredentials: true
      })
      .toPromise()
      .then((resp) => resp.json() as Group[]);
  }

  addGroup(groupName: string): Promise<any> {
    const body = new URLSearchParams();
    body.set('groupName', groupName);
    return this.http
      .post(this._baseUrl, body.toString(), {
          withCredentials: true, headers: this._headersUrl
        }
      )
      .toPromise()
      .then(response => response.json() as Group)
      .catch((error) => error = this.handleExistingGroup(error).error);
  }

  removeGroup(groupName: string): Promise<any> {
    return this.http.delete(this._baseUrl + `/remove/${groupName}`, {
      withCredentials: true, headers: this._headersUrl
    })
      .toPromise()
      .then((resp) => resp.json() as Group)
      .catch((error) => error = this.handleGroupRemoving(error).error);
  }

  private handleExistingGroup(error: any) {
    return Observable.throw('This group cannot be added.');
  }

  private handleGroupRemoving(error: any) {
    return Observable.throw('This group cannot be removed.');
  }
}
