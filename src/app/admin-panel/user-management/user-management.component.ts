import {Component, OnInit} from '@angular/core';
import {User} from '../../entities/user';
import {UserService} from '../user.service';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {MdSnackBar} from '@angular/material';
import {SectionExpansion} from '../../shared/section-expansion';
import 'rxjs/add/operator/startWith';
import {OidcService} from '../../oidc.service';
import {UserSectionExpansion} from '../../shared/user-section-expansion';


@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  currentUserId: string;
  users: User[] = [];
  mailFilter = {email: []};
  sectionExpansion: SectionExpansion;
  changedUser: User;

  constructor(private _userService: UserService,
              private _oidcService: OidcService,
              private snackBar: MdSnackBar) {
  }

  ngOnInit(): void {
    this.sectionExpansion = new SectionExpansion('More users');
    this._oidcService.getCurrentUser().then((user) =>
      this.currentUserId = user.id);
    this._userService.getAllUsers().then((users) => {
      this.users = users;
      this.users.sort(function (a, b) {
        return (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0);
      });
    }).then(() => this.sectionExpansion = UserSectionExpansion.loadUsers(this.users));
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  change(user: User) {
    this.users.filter((b) => b.id === user.id)[0].role = user.role;
    this.openSnackBar(user.name + ' was promoted to ' + user.role, '');
  }

  generateUserReport(userId: string) {
    this._userService.generateUserReport(userId);
  }

  generateStatistics() {
    this._userService.generateStatistics();
  }
}
