import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../entities/user';
import {UserService} from '../../user.service';
import {OidcService} from '../../../oidc.service';
import {Dir, MdSnackBar} from '@angular/material';
import {CustomSnackbar} from '../../../material/custom-snackbar';

@Component({
  selector: 'app-change-role',
  templateUrl: './change-role.component.html',
  styleUrls: ['./change-role.component.css']
})
export class ChangeRoleComponent implements OnInit {
  @Input() user: User;
  @Output() changeRole = new EventEmitter<User>();
  currentUserId: string;

  constructor(private _userService: UserService,
              private _oidcService: OidcService,
              private snackBar: MdSnackBar,
              private dir: Dir) {
  }

  ngOnInit() {
  }

  changeUserRole(role: string) {
    this._userService.changeRole(this.user.id, role)
      .then((user) => {
        if (user.role === role) {
          this.changeRole.emit(user);
          this.openSnackBar(user.name + ' is now ' + user.role.toLowerCase() + '.', '');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    const cs = new CustomSnackbar(this.snackBar, this.dir);
    cs.openSnackBar(message, action);
  }
}
