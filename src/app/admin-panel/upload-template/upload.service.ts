import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {MailTemplate} from '../../entities/mail-template';

@Injectable()
export class UploadService {
  apiEndPoint = 'api/templates';

  constructor(private _http: Http) {
  }

  uploadTemplate(formdata: FormData, options: RequestOptions): Promise<any> {
    return this._http.post(`${this.apiEndPoint}/upload`, formdata, options)
      .toPromise();
  }

  getAllTemplateTypes(): Promise<MailTemplate[]> {
    return this._http.get(`${this.apiEndPoint}/getAll`)
      .toPromise()
      .then(response => response.json() as MailTemplate[]);
  }

  downloadTemplate(templateType: string) {
    window.open(`api/template/download/${templateType}`, '_blank');
  }
}
