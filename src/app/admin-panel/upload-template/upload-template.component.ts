import {UploadService} from './upload.service';
import {Component, NgZone, OnInit} from '@angular/core';
import {Headers, RequestOptions} from '@angular/http';
import {MailTemplate} from '../../entities/mail-template';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-upload-template',
  templateUrl: './upload-template.component.html',
  styleUrls: ['./upload-template.component.css']
})

export class UploadTemplateComponent implements OnInit {
  apiEndPoint = 'api/templates/upload';
  submitted = false;
  show = true;
  subject: string;
  type: string;
  fileList: FileList;
  templates: MailTemplate[];
  showValue: string;
  subjectMinLength = 2;
  inFocus: boolean;

  constructor(private uploadService: UploadService, private zone: NgZone) {

  }

  ngOnInit() {
    this.getAllTemplateTypes();
    this.showValue = 'Download';
  }

  prepareFile(event) {
    this.fileList = event.target.files;
  }

  fileChange() {
    if (this.fileList.length > 0) {
      const file: File = this.fileList[0];
      const formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      const headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Subject', this.subject);
      headers.append('Type', this.type);
      const options = new RequestOptions({headers: headers});
      this.uploadService.uploadTemplate(formData, options);
      if (this.show === true) {
        this.show = !this.show;
      }
    }
  }

  onSubmit() {
    this.submitted = true;
    this.fileChange();
  }

  newUpload(): void {
    this.subject = '';
    this.type = null;
    this.fileList = null;
    this.submitted = false;
    this.show = false;
    this.showValue = 'Download';
  }

  validateForm(): boolean {
    if (this.subject !== undefined && this.subject.trim() !== '' && this.subject !== null
      && this.type !== undefined && this.type !== null
      && this.fileList !== undefined && this.fileList !== null && this.subject.length > this.subjectMinLength) {
      return true;
    }
    return false;
  }

  getAllTemplateTypes() {
    this.show = !this.show;
    if (this.showValue === 'Download') {
      this.showValue = 'Hide';
    } else {
      this.showValue = 'Download';
    }
    this.uploadService.getAllTemplateTypes()
      .then(templates => {
        this.zone.run(() => {
          this.templates = templates;
        });
      });
  }

  isEmptySubject(): boolean {
    return isNullOrUndefined(this.subject) || this.subject.trim() === '' || this.subject.length < this.subjectMinLength;
  }

  downloadTemplate(templateType: string) {
    this.uploadService.downloadTemplate(templateType);
  }
}
