import {Injectable} from '@angular/core';
import {Headers, Http, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {User} from '../entities/user';

@Injectable()
export class UserService {
  private _baseUrl = '/api/users';
  private _groupsUrl = '/api/user-groups';
  private _headersUrl = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private _http: Http) {
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }

  getAllUsers(): Promise<User[]> {
    return this._http.get(`${this._baseUrl}`, {
      withCredentials: true
    })
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }

  changeRole(id: string, newRole: string): Promise<User> {
    const body = new URLSearchParams();
    body.set('newRole', newRole);
    body.set('id', id);
    return this._http
      .patch(`api/users/changeRole`, body.toString(), {
        withCredentials: true, headers: this._headersUrl
      })
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  generateUserReport(userId: string) {
    window.open(`api/reports/user/${userId}`, '_blank');
  }

  generateStatistics(): any {
    window.open(`api/reports/statistics`, '_blank');
  }
}
